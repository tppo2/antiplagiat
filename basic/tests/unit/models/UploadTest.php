<?php

namespace tests\unit\models;

use app\models\UploadFile;
use yii\web\UploadedFile;
use yii\web;

class UploadFileTest extends \Codeception\Test\Unit
{
    private $model;
    protected $tester;

    public function testUploadFile(){
        $file = new UploadedFile([
            'name' => '1.php',
            'tempName' => \Yii::$app->basePath.'/tests/files/1.php',
            'type' => 'file/php',
            'size' => 84.71 * 1024]);
        $this->model = new UploadFile([
        ]);
        expect($this->model->upload(0, 0, 0, 'staryshe', 0, 0, $file));
    }


        public function testUploadIncorrectFile(){
        $file = new UploadedFile([
            'name' => '1.txt',
            'tempName' => \Yii::$app->basePath.'/tests/files/1.txt',
            'type' => 'file/txt',
            'size' => 84.71 * 1024]);
        $this->model = new UploadFile([
        ]);
        expect_not($this->model->upload(0, 0, 0, 'staryshe', 0, 0, $file));
    }

}