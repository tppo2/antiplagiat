<?php
use Yii;

class AuthFormCest
{
    protected $tester;

    public function _before(\FunctionalTester $I)
    {
        session_save_path(\yii::$app->basePath.'/sessions');
        $I->amOnRoute('auth/authr');
    }

    public function loginSuccessfully(\FunctionalTester $I)
    {
        $I->submitForm('#auth-form', [
            'AuthForm[login]' => 'staryshe',
            'AuthForm[pass]' => 'eeloo2Ei',
        ]);
        $I->amOnRoute('auth/mainpage');
        $I->see('Курсы', 'h1');        
    }


    public function loginWithWrongCredentials(\FunctionalTester $I)
    {
        $I->submitForm('#auth-form', [
            'AuthForm[login]' => 'sdgsdg',
            'AuthForm[pass]' => 'dsgsdg',
        ]);
        $I->amOnRoute('auth/mainpage');
        $I->dontSee('Курсы', 'h1');        
    }

}