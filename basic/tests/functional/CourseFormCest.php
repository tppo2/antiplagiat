<?php

use app\tests\fixtures\CourseFixture;
use app\tests\fixtures\UserFixture;
use app\tests\fixtures\TaskFixture;
use app\tests\fixtures\TaskItemFixture;

class CourseFormCest
{

    protected $tester;


    public function _fixtures(){
        return ['tblCourse'=>CourseFixture::className(), 'tblUser'=>UserFixture::className(), 'tblTask'=>TaskFixture::className(), 'tblTaskItem' => TaskItemFixture::className()];
    }

    public function _before(\FunctionalTester $I)
    {
        session_save_path(yii::$app->basePath.'/sessions');
        $I->amOnRoute('auth/authr');
        $I->submitForm('#auth-form', [
            'AuthForm[login]' => 'staryshe',
            'AuthForm[pass]' => 'eeloo2Ei',
        ]);
        $I->amOnRoute('auth/mainpage');
        $I->click('open_course_main_1');
    }

    public function openEditCourse(\FunctionalTester $I){
    	$I->click('edit_course_course_1');
    	$I->see('Сохранить');
    }

    public function deleteCourse(\FunctionalTester $I){
    	$I->see('NameCourse1');
    	$I->click('delete_course_course_1');
    	$I->dontSee('NameCourse1');
    }

    public function openTask(\FunctionalTester $I){
    	$I->click('open_task_course_1');
    	$I->see('Task1');
    	$I->see('Comment');
    }


}