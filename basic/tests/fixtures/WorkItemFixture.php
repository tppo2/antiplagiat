<?php

namespace tests\fixtures;

use yii\test\ActiveFixture;

class WorkItemFixture extends ActiveFixture
{
    public $modelClass = 'app\models\WorkItem';
}