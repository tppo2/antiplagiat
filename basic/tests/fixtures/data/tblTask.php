<?php
    return [
        'task1' => [
            'idTask' => 1,
            'idCourse' => 1,
            'Task' => 'Task1',
            'Comment' => 'Comment', 
            'Uniqueness' => 70,
            'Year' => 5,
            'Tries' => 3,   
            'FilesQuantity' => 2,
            'isDeleted' => 0,
            ],       
    ];