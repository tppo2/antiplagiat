import sys

def sravniSomeFiles(strfile, strfile2):
    f = open(strfile, "r", encoding="utf-8")
    raw1 = f.read()
    f = open(strfile2, "r", encoding="utf-8")
    raw2 = f.read()

    n = len(raw1)
    m = len(raw2)
    F = [[0] * (m + 1) for i in range(n + 1)]
    for i in range(1, n + 1):
        for j in range(1, m + 1):
            if raw1[i - 1] == raw2[j - 1]:
                F[i][j] = F[i - 1][j - 1] + 1
            else:
                F[i][j] = max(F[i - 1][j], F[i][j - 1])

    original = 100-((F[n][m]/m)*100)
    return original

print(sravniSomeFiles(sys.argv[1], sys.argv[2]))