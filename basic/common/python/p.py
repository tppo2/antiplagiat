#!/usr/bin/env python2
import ldap3
import sys
import getpass
from ldap3.utils import config

LDAP_SERVER = 'ldaps://ldap.cs.prv'
LDAP_PORT = 636
LDAP_USE_SSL = True
ldap_server = ldap3.Server("192.168.112.1", use_ssl=True, port=636, get_info='ALL')


def is_student(dn):
    if(dn.find("students") != -1):
        return True
    else:
        return False

def get_name(dn):
    return dn[3:dn.find(',')]

def is_in_database(uid):
    return True


def add_to_database(dn):
    return False


def login_user(uid="", password=""):
    result = False
    name = ''
    if uid is None or password is None:
        return result
    c = ldap3.Connection(ldap_server)
    c.bind()
    c.search(search_base="OU=people,DC=cs,DC=karelia,DC=ru", search_filter="(uid={})".format(uid), attributes=[])
    name = get_name(c.response[0]['dn'])
    stud = is_student(c.response[0]['dn'])
    if c.rebind(user=c.response[0]['dn'], password=password):
        result = True
        if  not is_in_database(uid):
            add_to_database(c.response[0]['dn'])
    else:
        result=False
    c.unbind()
    return result, stud, name


#login = input("Login: ")
#password = getpass.getpass()
login = sys.argv[1]
password = sys.argv[2]
result, stud, name = login_user(login, password)
print(result, stud, name)
