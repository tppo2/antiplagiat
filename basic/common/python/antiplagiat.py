#!/usr/bin/env python3
import sys
import nltk


def sravniFiles(strfile, strfile2):
    def out_purple(text):
        print("\033[35m {}".format(text))

    f = open(strfile, "r", encoding="utf-8")
    raw = f.read()

    i = 0
    j = 0
    string = ''

    while i < len(raw) - 1:
        if raw[i] == "<" and raw[i + 1] == "?" and raw[i + 2] == "p" and raw[i + 3] == "h" and raw[i + 4] == "p":
            j = i
        elif (raw[i] == "?" and raw[i + 1] == ">") or i == len(raw) - 2 :
            k = i + 3
            string += raw[j:k]
        i += 1

    tokens = nltk.word_tokenize(string)

    i = 0
    while i < len(tokens):
        if tokens[i] == 'php':
            del tokens[i]
            break
        del tokens[i]

    if tokens[len(tokens) - 2] == '?' and tokens[len(tokens) - 1] == '>':
        i = len(tokens) - 1
        while tokens[i] != '?':
            del tokens[i]
            i -= 1
        del tokens[i]


    i = 0
    while i < len(tokens):
        if tokens[i] == "'" or tokens[i].startswith("'"):
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == "'" or tokens[i].endswith("'"):
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i] == '"' or tokens[i].startswith('"'):
            tokens[i] = 'CHISLO'
            i += 1
            while i < len(tokens):
                if tokens[i] == '"' or tokens[i].endswith('"'):
                    del tokens[i]
                    break
                else:
                    del tokens[i]
        elif tokens[i].isdigit():
            tokens[i] = 'CHISLO'
            i += 1
        elif tokens[i] == '$':
            tokens[i] = 'IDEN'
            del tokens[i + 1]
        elif tokens[i].casefold() == "true" or tokens[i].casefold() == 'false':
            tokens[i] = 'CHISLO'
        elif tokens[i] == ":" or tokens[i] == "!" or tokens[i] == "<" or tokens[i] == ">" or tokens[i] == "php" or \
                tokens[i] \
                == '?':
            del tokens[i]
        elif tokens[i] == "(" or tokens[i] == ")" or tokens[i] == "[" or tokens[i] == "]" or tokens[i] == "}" or tokens[
            i] == "{":
            tokens[i] = 'SKOB'
        elif tokens[i] == "=" or tokens[i] == "+" or tokens[i] == "-" or tokens[i] == "/" or tokens[i] == "*" or tokens[
            i] == "==" or tokens[i] == "<=" or tokens[i] == ">=" or tokens[i] == "!=":
            tokens[i] = 'OPER'
        elif tokens[i] == ";":
            tokens[i] = 'PER'
        elif tokens[i] == "&" or tokens[i] == "|" or tokens[i].casefold() == "if" or tokens[i].casefold() == "else" or \
                tokens[i].casefold() == "echo" or tokens[i].casefold() == "elseif":
            tokens[i] = 'KL'
            if tokens[i + 1] == "&" or tokens[i + 1] == "|":
                del tokens[i + 1]
        else:
            i += 1

    i = 0
    while i < len(tokens):
        if tokens[i] != 'IDEN' and tokens[i] != 'OPER' and tokens[i] != 'KL' and tokens[i] != 'CHISLO' and tokens[
            i] != 'SKOB' and tokens[i] != 'PER':
            tokens[i] = 'KL'
            i += 1
        else:
            i += 1

    # CHISLO - 1 - числовое значение/сивол/набор символов
    # IDEN - 2 - идентификатор ($per)
    # KL - 3 - ключевое слово языка
    # OPER - 4 - операции
    # PER - 5 - перенос строки
    # SKOB - 6 - скобка

    for i in range(0, len(tokens)):
        if tokens[i] == 'CHISLO':
            tokens[i] = '1'
        elif tokens[i] == 'IDEN':
            tokens[i] = '2'
        elif tokens[i] == 'KL':
            tokens[i] = '3'
        elif tokens[i] == 'OPER':
            tokens[i] = '4'
        elif tokens[i] == 'PER':
            tokens[i] = '5'
        elif tokens[i] == 'SKOB':
            tokens[i] = '6'

    grafs = [0] * (len(tokens) - 3)

    i = 0
    while i + 3 < len(tokens):
        grafs[i] = tokens[i] + tokens[i + 1] + tokens[i + 2] + tokens[i + 3]
        i += 1

    i = 0
    while i < len(grafs) - 1:
        j = 0
        while j < len(grafs):
            if grafs[i] == grafs[j] and (i != j):
                del grafs[j]
                j -= 1
            j += 1
        i += 1

    # Проверка

    # i = 0
    # flag = 0
    # while i < len(grafs):
    #    j = 0
    #    while j < len(grafs):
    #        if grafs[i] == grafs[j] and (i != j):
    #            flag = 1
    #        j += 1
    #    i += 1

    # Далее код получения токенов каждого файла нужно вынести в цикл

    f = open(strfile2, "r", encoding="utf-8")
    raw = f.read()

    i = 0
    j = 0
    string = ''

    while i < len(raw) - 1:
        if raw[i] == "<" and raw[i + 1] == "?" and raw[i + 2] == "p" and raw[i + 3] == "h" and raw[i + 4] == "p":
            j = i
        elif (raw[i] == "?" and raw[i + 1] == ">") or i == len(raw) - 2 :
            k = i + 3
            string += raw[j:k]
        i += 1



    tokenssrav = nltk.word_tokenize(string)

    i = 0
    while i < len(tokenssrav):
        if tokenssrav[i] == 'php':
            del tokenssrav[i]
            break
        del tokenssrav[i]

    if tokenssrav[len(tokenssrav) - 2] == '?' and tokenssrav[len(tokenssrav) - 1] == '>':
        i = len(tokenssrav) - 1
        while tokenssrav[i] != '?':
            del tokenssrav[i]
            i -= 1
        del tokenssrav[i]

    i = 0
    while i < len(tokenssrav):
        if tokenssrav[i] == "'" or tokenssrav[i].startswith("'"):
            tokenssrav[i] = 'CHISLO'
            i += 1
            while i < len(tokenssrav):
                if tokenssrav[i] == "'" or tokenssrav[i].endswith("'"):
                    del tokenssrav[i]
                    break
                else:
                    del tokenssrav[i]
        elif tokenssrav[i] == '"' or tokenssrav[i].startswith('"'):
            tokenssrav[i] = 'CHISLO'
            i += 1
            while i < len(tokenssrav):
                if tokenssrav[i] == '"' or tokenssrav[i].endswith('"'):
                    del tokenssrav[i]
                    break
                else:
                    del tokenssrav[i]
        elif tokenssrav[i].isdigit():
            tokenssrav[i] = 'CHISLO'
            i += 1
        elif tokenssrav[i] == '$':
            tokenssrav[i] = 'IDEN'
            del tokenssrav[i + 1]
        elif tokenssrav[i].casefold() == "true" or tokenssrav[i].casefold() == 'false':
            tokenssrav[i] = 'CHISLO'
        elif tokenssrav[i] == ":" or tokenssrav[i] == "!" or tokenssrav[i] == "<" or tokenssrav[i] == ">" or tokenssrav[
            i] == "php" or tokenssrav[i] \
                == '?':
            del tokenssrav[i]
        elif tokenssrav[i] == "(" or tokenssrav[i] == ")" or tokenssrav[i] == "[" or tokenssrav[i] == "]" or tokenssrav[
            i] == "}" or tokenssrav[
            i] == "{":
            tokenssrav[i] = 'SKOB'
        elif tokenssrav[i] == "=" or tokenssrav[i] == "+" or tokenssrav[i] == "-" or tokenssrav[i] == "/" or tokenssrav[
            i] == "*" or tokenssrav[
            i] == "==" or tokenssrav[i] == "<=" or tokenssrav[i] == ">=" or tokenssrav[i] == "!=":
            tokenssrav[i] = 'OPER'
        elif tokenssrav[i] == ";":
            tokenssrav[i] = 'PER'
        elif tokenssrav[i] == "&" or tokenssrav[i] == "|" or tokenssrav[i].casefold() == "if" or tokenssrav[
            i].casefold() == "else" or \
                tokenssrav[i].casefold() == "echo" or tokenssrav[i].casefold() == "elseif":
            tokenssrav[i] = 'KL'
            if tokenssrav[i + 1] == "&" or tokenssrav[i + 1] == "|":
                del tokenssrav[i + 1]
        else:
            i += 1

    i = 0
    while i < len(tokenssrav):
        if tokenssrav[i] != 'IDEN' and tokenssrav[i] != 'OPER' and tokenssrav[i] != 'KL' and tokenssrav[
            i] != 'CHISLO' and tokenssrav[
            i] != 'SKOB' and tokenssrav[i] != 'PER':
            tokenssrav[i] = 'KL'
            i += 1
        else:
            i += 1

    for i in range(0, len(tokenssrav)):
        if tokenssrav[i] == 'CHISLO':
            tokenssrav[i] = '1'
        elif tokenssrav[i] == 'IDEN':
            tokenssrav[i] = '2'
        elif tokenssrav[i] == 'KL':
            tokenssrav[i] = '3'
        elif tokenssrav[i] == 'OPER':
            tokenssrav[i] = '4'
        elif tokenssrav[i] == 'PER':
            tokenssrav[i] = '5'
        elif tokenssrav[i] == 'SKOB':
            tokenssrav[i] = '6'

    grafssrav = [0] * (len(tokenssrav) - 3)

    i = 0
    while i + 3 < len(tokenssrav):
        grafssrav[i] = tokenssrav[i] + tokenssrav[i + 1] + tokenssrav[i + 2] + tokenssrav[i + 3]
        i += 1

    i = 0
    while i < len(grafssrav) - 1:
        j = 0
        while j < len(grafssrav):
            if grafssrav[i] == grafssrav[j] and (i != j):
                del grafssrav[j]
                j -= 1
            j += 1
        i += 1

    # Проверка
    # i = 0
    # flag = 0
    # while i < len(grafssrav):
    #    j = 0
    #    while j < len(grafssrav):
    #        if grafssrav[i] == grafssrav[j] and (i != j):
    #            flag = 1
    #        j += 1
    #    i += 1

    #out_purple(grafs)
    #out_purple(grafssrav)

    koef = 0
    i = 0
    while i < len(grafs) - 1:
        j = 0
        while j < len(grafssrav):
            if grafs[i] == grafssrav[j]:
                koef += 1
            j += 1
        i += 1

    k = len(grafs)
    # print(k)
    original = 100 - ((koef / (k - 1)) * 100)

    return original


nltk.download('punkt')
print(sravniFiles(sys.argv[1], sys.argv[2]))