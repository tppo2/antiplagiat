<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
 
class UploadFile extends Model{
 
    public $file1;
 
public function rules(){
	return[
	    //[['file1'], 'skipOnEmpty'=>false, 'required'],
	    //[['file1'], 'skipOnEmpty'=>false, 'file', 'extensions' => 'php']
	];

	return[
		[['file1'], 'file', 'skipOnEmpty'=>false, 'extensions'=>'php'],
	];
    } 

    public function upload($number, $idcourse, $idtask, $username, $version, $num, UploadedFile $file){
	if ($this->validate())
	{
		$this->file1[$number] = $file;
		$filename =  $num . '.' . $file->extension;
        	$res = $file->saveAs('uploads/' . $idcourse . '/' . $idtask . '/' . $username . '/' . $version . '/' . $filename);
  					
		return $filename;
	}
	else
	{
		return false;
	}
    }


 
}