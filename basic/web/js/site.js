function showAllCourses(){
	courses=document.getElementById('courses-scroll');
	button=document.getElementById('show-all');
	if (courses.style.overflow === "auto"){
		courses.style.overflow = 'visible';
		courses.style.height = 'auto';
		courses.style.minHeight = '400px';
		button.innerHTML = 'Скрыть';
	}
	else{
		courses.style.overflow = "auto";
		courses.style.height = '400px';
		button.innerHTML = 'Показать все курсы';
		courses.style.minHeight = 'auto';
	}
}