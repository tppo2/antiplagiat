<?php
require_once "PhpLibrary.php";
session_start();
if (empty($_SESSION["login"])) {
	echo "<script>document.location.replace('index.php');</script>"; 
	exit();
}
$usr = $_SESSION["login"];
$error_consultation_1 = "";
$error_link_1 = "";
$error_literature_1 = "";
$error_scedule_1 = "";
$error_literature_2 = "fdfdfdfdfdfdfd";
if (isset($_POST["send4"])) {
	session_destroy();
	echo "<script>document.location.replace('index.php');</script>";
	exit();
}
if (isset($_POST["BackSubmit"])) {
	echo "<script>document.location.replace('Distributor.php');</script>";    
	exit();
}
?>
<!Doctype html>
<html lang="ru"> 

<head>
	<title> Конструктор сайтов </title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<style>
    h2{
		font-size:35px;
	    display: flex;
        align-items: center;
        justify-content: center;
		color:blue;
	  }
	  h3{
	      font-size:25px;
	      font-family:calibri; 
	  }
    </style>
</head>

<body>
    <div class="floating_window">
         <svg class="modal__cross" xmlns="http://www.w3.org/2000/svg"               viewBox="0 0 24 24"><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/></svg>
         <div id = "content_of_modal_window"></div>
    </div>
    <div class="overlay"></div>
	<div id="content">
	    <div style="width:100%; position: fixed; top: 0; height:135px; z-index:5" >
		<header id="headblock" style="height:120px; text-align:center;">
			<h1 class="head" style="height:30px">Заполните форму</h1>
			<div class="tab">
			    <button style="height:auto" class="tablinks active" onclick="openBlock(event, 'information_of_discipline')">Информация о дисциплине</button>
                <button style="height:auto" class="tablinks" onclick="openBlock(event, 'study_process')">Учебный процесс</button>
                <button style="height:auto" class="tablinks" onclick="openBlock(event, 'electronic_resources')">Учебно-методические ресурсы</button>
		    </div>
		</header><br />
		<div id="exit" style="background-color:transparent; height:30px">
			<form name="exit" method="post">
				<input id="butt" type="submit" style="width:80px" name="send4" value="выйти" />
				<label>Пользователь: <?php echo $_SESSION["login"] ?> </label>
				<input id="backButton" type="submit" name="BackSubmit" style="width:80px" value="назад" />
			</form>
		</div>
		</div>
		<form id="feedback" style="margin-top:135px">
			<div class="block" id="information_of_discipline" style="display:block">
				<a class="note">Необходимо заполнить все поля ввода, отмеченные символом *!</a><br />
				<h2 class="h2_constr"><span class="imag4"></span> &nbsp;Информация о дисциплине</h2>
				<div id="disciple">
				    <label> <span class="simple_text">Фамилия преподавателя*</span> </label><br /><input type="text" name="fio" style="width:350px" required /><label id="fio_error" class="error"></label><br />
					<br />
					<label> <span class="simple_text">Имя преподавателя*</span> </label><br /><input type="text" style="width:350px" required name="nam" /><label id="nam_error" class="error"></label><br />
					<br />
					<label> <span class="simple_text">Отчество преподавателя*</span> </label><br /><input type="text" style="width:350px" required name="fath" /><label id="fath_error" class="error"></label><br />
					<br />
					<label> <span class="simple_text">Наименование предмета*</span></label><br /><input type="text" style="width:350px" name="disc" required /><label id="disc_error" class="error"></label><br />
					<br />
					<label> <span class="simple_text">Кафедра*</span></label><br />
					<select name="Kafedra" style="width:360px" >
						<?php
						$f = fopen('data/Kafedras.txt', 'r');
						$line = fgets($f);
						while ($line) {
							echo "<option name=" . $line . ">" . $line . "</option>"; 
							$line = fgets($f);
						}
						fclose($f)
						?>
					</select><br />
					<br />
					<label> <span class="simple_text">Тип дисциплины*</span></label><br />
					<select name="type" style="width:360px">
						<?php
						$f = fopen('data/TypesOfDisciples.txt', 'r');
						$line = fgets($f);
						while ($line) {
							echo "<option name=" . $line . ">" . $line . "</option>"; 
							$line = fgets($f);
						}
						fclose($f)
						?>
					</select><br />
					<br />
					<label> <span class="simple_text">Специальность*</span></label><br />
					<select name="direction" style="width:360px">
						<?php
						$f = fopen('data/DirectionsOfStudy.txt', 'r');
						$line = fgets($f);
						while ($line) {
							echo "<option name=" . $line . ">" . $line . "</option>";
							$line = fgets($f);
						}
						fclose($f)
						?>
					</select><br />
					<br />
					<label> <span class="simple_text">Курс*</span></label><br /><input style="width:350px" type="number" name="course" required /><label id="course_error" class="error"></label><br />
					<br />
					<label> <span class="simple_text">Семестры*</span></label><br /><input style="width:350px"" type="number" name="semester" required /><label id="semester_error" class="error"></label><br />
					<br />
					<label> <span class="simple_text">Контроль*</span></label><br /><input style="width:350px" type="text" name="control" required /><label id="control_error" class="error"></label><br />
					<br />
					<button type="button" title="Предпросмотр" class="preview" id="preview1">👁</button>
				</div>
			</div>
			<div class="block" id="study_process">
				<h2 class="h2_constr"><span class="imag5"></span> &nbsp;Учебный процесс</h2>
				<a class="note">Необязательные поля для заполнения</a><br />
				<h3 class="event-click" id="sublime1">Консультации</h3>
				<div class="stud">
				    <table id="parentHD" style="width:100%">
					        <thead>
					        <tr><th><span class="simple_table_header">День недели</span></th><th><span class="simple_table_header">Время</span></th><th><span class="simple_table_header">Преподаватель</span></th><th><span class="simple_table_header">Аудитория</span></th><th><span class="simple_table_header">on-line</span></th></tr>
						    </thead>
						    <tbody>
						        <tr><th><select name="den_1" style="width:150px;height:30px; font-size:15px">
								<?php
								$f = fopen('data/DaysOfWeek.txt', 'r');
								$line = fgets($f);
								while ($line) {
									echo "<option name=" . $line . ">" . $line . "</option>";
									$line = fgets($f);
								}
								fclose($f)
								?>
							</select></th>
							<th><input type="time" name="timee_1" style="height:30px; font-size:15px"/></th><th><input type="text" name="teacher_1" style="width:150px;height:22px; font-size:15px"/></th><th><input style="width:60px;height:22px" type="number" name="class_1" /></th><th><input type="text" name="online_1" style="width:150px;height:22px; font-size:15px"/></th><th></th></th></tr>
						    </tbody>
						</table>
					<a onclick="return addField('parentHD')" href="#">Добавить консультацию</a>
					<button type="button" title="Предпросмотр" class="preview" id="preview2">👁</button>
					<br />
				</div>
				<h3 class="event-click2" id="sublime2">Расписание</h3>
				<div class="schedule">
					<table id="parentSub" style="width:100%">
					        <thead>
					        <tr><th><span class="simple_table_header">День недели</span></th><th><span class="simple_table_header">Время</span></th><th><span class="simple_table_header">Преподаватель</span></th><th><span class="simple_table_header">Аудитория</span></th><th><span class="simple_table_header">on-line</span></th></tr>
						    </thead>
						    <tbody>
						        <tr><th><select name="den_1" style="width:150px;height:30px; font-size:15px">
								<?php
								$f = fopen('data/DaysOfWeek.txt', 'r');
								$line = fgets($f);
								while ($line) {
									echo "<option name=" . $line . ">" . $line . "</option>";
									$line = fgets($f);
								}
								fclose($f)
								?>
							</select></th>
							<th><input type="time" style="height:30px; font-size:15px" name="timeOfSub_1" /></th><th><input type="text" name="teacherOfSub_1" style="width:150px;height:22px; font-size:15px"/></th><th><input style="width:60px;height:22px" type="number" name="classOfSub_1" /></th><th><input style="width:150px;height:22px; font-size:15px" type="text" name="onlineOfSub_1" /></th><th></th></th></tr>
						    </tbody>
						</table>
					<a onclick="return addField('parentSub')" href="#">Добавить поле</a>
					<button type="button" title="Предпросмотр" class="preview" id="preview3">👁</button>
					<br />
				</div>
				<h3 class="event-click3" id="sublime3">Учебный план</h3>
				<div class="teachingPlan">
					<table id="parentPlan" style="width:100%">
					        <thead>
					        <tr><th><span class="simple_table_header">Тематика занятий</span></th><th><span class="simple_table_header">Количество часов</span></th><th><a onclick="return addColumn()" href="#">[+]</a></th></tr>
						    </thead>
						    <tbody>
						        <tr></th>
							<th><input type="text" name="topicOfLesson_1" style="height:22px; font-size:15px"/></th><th><input type="number" name="countOfHours_1" style="width:150px;height:22px; font-size:15px"/></th><th></th></tr>
						    </tbody>
						</table>
					<br />
					<a onclick="return addField('parentPlan')" href="#">Добавить поле</a>
				</div>
				<br />
			</div>
			<div class="block" id="electronic_resources">
				<h2 class="h2_constr"><span class="imag6"></span> &nbsp;Учебно-методические ресурсы</h2>
				<a class="note">Данные об одной ссылке и об одном файле должны быть заполнены полностью</a><br />
				<h3>Список литературы</h3>
				<div class="source" id="bibliography2" style="width:1400px;  overflow-wrap: break-word; word-wrap: break-word; word-break: break-all; word-break: break-word; hyphens: auto;">
				        <b><span class="simple_strong_text">Пример библиографического описания:</span></b><span class = "simple_text" style="overflow-wrap: break-word; word-wrap: break-word; word-break: break-all; word-break: break-word; hyphens: auto;" > Решение дифференциальных уравнений в системе компьютерной математики Maxima Губина Т. Н. , Андропова Е. В. . - Елец.:Елецкий государственный университет им. И. А. Бунина,2009.345</span><br />
				        <b class="simple_strong_text">Список библиографических описаний для вашего сайта:</b><label id="bibliography2_1_error" class="error"></label><br />
				        <div id="biblographyList"></div>
						<br /><a onclick="return addBibliography()" href="#">Добавить библиографическое описание</a><br />
						<button type="button" title="Предпросмотр" class="preview" id="preview10">👁</button>
				</div>
				<br />
				<h3>Ссылки на источники</h3>
				<div class="source" id="Links">
					<br />
					    <table id="parentLink" style="width:100%">
					        <thead>
					        <tr><th><span class="simple_table_header">Описание ссылки</span></th><th><span class="simple_table_header">Ссылка</span></th></tr>
						    </thead>
						    <tbody>
						        <tr><th><textarea style="width:250px;resize:vertical"  name="hyperName_1"></textarea></th><th><input style="width:400px" type="text" required name="hyper_1"></th><th></th><th align="center" valign="top"><label id="hyper_1_error" class="error"></label></th></tr>
						    </tbody>
						</table>
					<a onclick="return addField('parentLink')" href="#">Добавить ссылку</a><br />
					<button type="button" title="Предпросмотр" class="preview" id="preview5">👁</button>
					<br />
				</div>
				<br />
				<h3>Электронные документы</h3>
				<a class="note">Запрещённые расширения загружаемых файлов:html, css, php, js, sql </a><br />
				<div class="source" id="ElectronicDocuments">
					<br />
					<div id="parentBook">
						<div>
						    <table>
						    <thead>
					        <tr><th><span class="simple_table_header">Название документа</span></th><th><span class="simple_table_header">Файл</span></th></tr>
						    </thead><tbody>
						    <th><textarea style="width:250px;resize:vertical" required  name="book_1"></textarea></th><th><input  type="file" required name="filename_1"></th><th><label id="filename_1_error" class="error"></label></th><br /></tbody></table>
						</div>
						<span class="head" style="color: red"><?= $error_literature_1 ?></span>
					</div>
					<br /><a onclick="return addField('parentBook')" href="#">Добавить источник</a><br />
					<button type="button" title="Предпросмотр" class="preview" id="preview6">👁</button>
				</div>
			</div>
			<br />
		</form>
		<div id="Create" style="text-align: center;"><button style="width:100px; margin:0 auto" type="button" name="send" id="send">Создать сайт</button><br />
		</div>
	</div>
	<footer id="footerBlock"><a class="information_of_developer">Разработчик: Никита Калинин</a></footer>
	</body>
</html>
<script type="text/javascript" language="javascript">
	var countOfFields = 1; // Текущее число полей
	var curFieldNameId = 1; // Уникальное значение для атрибута name
	var maxFieldLimit = 5; // Максимальное число возможных полей
	var curLinkNameId = 1;
	var curBibliographyName = 1;
	var countOfLinks = 1;
	var curSubNameId = 1;
	var countOfSubs = 1;
	var curBookNameId = 1;
	var countOfBooks = 1;
	var countOfBibliography = 1;
	var arrayOfLinks = [1, 0, 0, 0, 0];
	var arrayOfSubs = [1, 0, 0, 0, 0];
	var arrayOfHD = [1, 0, 0, 0, 0];
	var arrayOfBooks = [1, 0, 0, 0, 0];
	var arrayOfBibliography = [1, 0, 0, 0, 0];
	var countOfBibliography2=0;
	let arrayOfBibliography2 = [[], [], [], [], []];
	var arrayOfColumnNamesOfPlan = [['topicOfLesson'],['countOfHours']];
	var arrayOfColumnTypesOfPlan = [['inputText'],['inputNumber']];
	var arrayOfColumnRealNames = [['Тематика занятий'], ['Количество часов']];
	var countOfPlans = 1;
	var curPlanNameId = 1;
	var arrayOfPlans = [1, 0, 0, 0, 0];
	var countOfColumnOfPlan = 3;
	function contentEdit(e, index){
		if(e.innerHTML == ""){
			e.innerHTML = "Редактируемое поле";
		}
		arrayOfColumnRealNames[index-1] = e.innerHTML;
		alert(arrayOfColumnRealNames); 
	}
	function deleteRowOfPlan(a,index){
		ell = a.parentNode.parentNode; // tr element (ваша строчка)
        ell.parentNode.removeChild(ell);
	    countOfPlans--;
		arrayOfPlans[index] = 0;
	}
	function deleteColumn(index){
		var table = document.querySelectorAll('#parentPlan')[0];
        var rows = table.querySelectorAll('tr');
        var col = table.querySelectorAll('th');
	    for (var i = 0; i < rows.length; i++) {
            var count = index + 1;
            col[(i)*countOfColumnOfPlan + index - 1].remove(); 
			if(index != countOfColumnOfPlan-1  && i!=0){
				for(let j=index-1; j<countOfColumnOfPlan-1;j++){
					col[(i)*countOfColumnOfPlan + j].children[0].id = "newColumn_"+j+"_"+i;
				}
			}
			else if(index != countOfColumnOfPlan-1  && i==0){ 
				for(let j=index-1; j<countOfColumnOfPlan-1;j++){ 
					var currentCount = j;
					col[(i)*countOfColumnOfPlan + j].innerHTML = `<div class="div_ul" style="height:100%;width:100%"><ul class="menu_ul" >
                	<li class="menu_li" style="height:25px">
                    	<a href="#" class="simple_table_header"><span onblur="contentEdit(this,`+countOfColumnOfPlan+`)" style="width:50px;height:25px"contenteditable="true" >`+arrayOfColumnRealNames[currentCount]+`</span>▼</a>
                	<ul class="menu_ul" >
                		<li class="menu_li">
                    		<a href="#" onclick="deleteColumn(`+currentCount+`)">Удалить колонку</a>
                		</li>
                		<li class="menu_li">
                    		<a href="#">Изменить тип полей</a>
                    		<ul class="menu_ul second_menu_ul" style="top:0px; left:105px">
                    			<li class="menu_li">
                        			<a href="#" onclick="changeColumnType(`+currentCount+`, 'Текст')">Текст</a>
                    			</li>
                    			<li class="menu_li">
                        			<a href="#" onclick="changeColumnType(`+currentCount+`, 'Число')">Число</a>
                    			</li>
                    			<li class="menu_li">
                        			<a href="#" onclick="changeColumnType(`+currentCount+`, 'Файл')">Файл</a>
                    			</li>
                    		</ul>
                		</li>
                	</ul>
            		</li>
            		</ul></div>`;
				}
			}
        }
		countOfColumnOfPlan--;
		arrayOfColumnNamesOfPlan.splice(index-1,1);
		arrayOfColumnTypesOfPlan.splice(index-1,1);
		arrayOfColumnRealNames.splice(index-1,1);
		if (index != countOfColumnOfPlan){
			for(let j=index-1; j<countOfColumnOfPlan-1;j++){
				let c = j + 1;
				arrayOfColumnNamesOfPlan[j] = "newColumn_"+c;
				//alert(arrayOfColumnNamesOfPlan[j]);
			}
		}
	}
	function changeColumnType(index, type){
	    var table = document.querySelectorAll('#parentPlan')[0]; 
        var rows = table.querySelectorAll('tr');
        var col = table.querySelectorAll('th');
	    for (var i = 0; i < rows.length; i++) {
	        if(i!=0){
            var count = index + 1;
            if (type == "Файл"){
                col[(i)*countOfColumnOfPlan + index - 1].innerHTML = "<input type=\"file\" style=\"font-size:15px\" id=\"newColumn_"+count+"_"+i+"\">";
                arrayOfColumnTypesOfPlan[index - 1] = "inputFile";
            }
            else if(type == "Текст"){
                col[(i)*countOfColumnOfPlan + index - 1].innerHTML = "<input type=\"text\" style=\"height:22px; font-size:15px\" id=\"newColumn_"+count+"_"+i+"\">";
                arrayOfColumnTypesOfPlan[index - 1] = "inputText";
            }
            else if(type == "Число"){
                col[(i)*countOfColumnOfPlan + index - 1].innerHTML = "<input type=\"number\" style=\"height:22px; font-size:15px;width:150px\" id=\"newColumn_"+count+"_"+i+"\">";
                arrayOfColumnTypesOfPlan[index - 1] = "inputNumber";
            }
            }
        }
	}
	function addColumn(){
	    if(countOfColumnOfPlan >=5){
            alert("Максимальное число колонок достигнуто");
            return false;
        }
		arrayOfColumnRealNames.push("Редактируемое поле");
	    var table = document.querySelectorAll('#parentPlan')[0];
        var rows = table.querySelectorAll('tr');
        var col = table.querySelectorAll('th');
        for (var i = 0; i < rows.length; i++) {
            var newCol = col[(i+1)*countOfColumnOfPlan-1].cloneNode(true);
            if (i==0){
                /*col[(i+1)*countOfColumnOfPlan-1].innerHTML = "<div class=\"saitbar\"><ul class=\"menu-4\"><li><a href=\"#\"  class=\"simple_table_header\"  ><span placeholder=\"Поле\" style=\"width:50px;\"contenteditable=\"true\">Поле</span> ▼</a>\
                    <ul class=\"podmenu\">\
                    <li><a href=\"#\" onclick=\"changeColumnType("+countOfColumnOfPlan+", 'Текст')\">Текст</a></li>\
                    <li><a href=\"#\" onclick=\"changeColumnType("+countOfColumnOfPlan+", 'Файл')\">Файл</a></li>\
                    <li><a href=\"#\" onclick=\"changeColumnType("+countOfColumnOfPlan+", 'Число')\">Число</a></li>\
                </ul>\
                </li></ul></div>";*/
                /*col[(i+1)*countOfColumnOfPlan-1].innerHTML = "<div class=\"saitbar\"><ul class=\"menu-4\"><li><a href=\"#\"  class=\"simple_table_header\"  ><span placeholder=\"Поле\" style=\"width:50px;\"contenteditable=\"true\">Поле</span> ▼</a>\
                    <ul class=\"podmenu\">\
                    <li>\
                        <a href=\"#\" >Изменить тип колонки</a>\
                    <ul class=\"podmenu\">\
                    <li><a href=\"#\" onclick=\"changeColumnType("+countOfColumnOfPlan+", 'Текст')\">Текст</a></li>\
                    <li><a href=\"#\" onclick=\"changeColumnType("+countOfColumnOfPlan+", 'Файл')\">Файл</a></li>\
                    <li><a href=\"#\" onclick=\"changeColumnType("+countOfColumnOfPlan+", 'Число')\">Число</a></li>\
                    </ul>\
                    </li>\
                    <li><a href=\"#\" >Удалить колонку</a></li>\
                </ul>\
                </li></ul></div>";*/
                col[(i+1)*countOfColumnOfPlan-1].innerHTML =`<div class="div_ul" style="height:100%;width:100%"><ul class="menu_ul" >
                <li class="menu_li" style="height:25px">
                    <a href="#" class="simple_table_header"><span onblur="contentEdit(this,`+countOfColumnOfPlan+`)" style="width:50px;height:25px"contenteditable="true" >Редактируемое поле</span>▼</a>
                <ul class="menu_ul" >
                <li class="menu_li">
                    <a href="#" onclick="deleteColumn(`+countOfColumnOfPlan+`)">Удалить колонку</a> 
                </li>
                <li class="menu_li">
                    <a href="#">Изменить тип полей</a>
                    <ul class="menu_ul second_menu_ul" style="top:0px; left:105px">
                    <li class="menu_li">
                        <a href="#" onclick="changeColumnType(`+countOfColumnOfPlan+`, 'Текст')">Текст</a>
                    </li>
                    <li class="menu_li">
                        <a href="#" onclick="changeColumnType(`+countOfColumnOfPlan+`, 'Число')">Число</a>
                    </li>
                    <li class="menu_li">
                        <a href="#" onclick="changeColumnType(`+countOfColumnOfPlan+`, 'Файл')">Файл</a>
                    </li>
                    </ul>
                </li>
                </ul>
            </li>
            </ul></div>`;
                
            }
            else{
                var count = countOfColumnOfPlan +1;
                col[(i+1)*countOfColumnOfPlan-1].innerHTML = "<input type=\"text\" style=\"height:22px; font-size:15px\" id=\"newColumn_"+countOfColumnOfPlan+"_"+i+"\">";
            }
            rows[i].appendChild(newCol); 
        }
        arrayOfColumnTypesOfPlan.push(['inputText']);
        var count = countOfColumnOfPlan;
        arrayOfColumnNamesOfPlan.push(['newColumn_'+count]);
		countOfColumnOfPlan+=1;
    }
    function addBibliography(){
        if(countOfBibliography2>5){
            alert("Число полей достигло своего максимума = " + maxFieldLimit);
            return false;
        }
        countOfBibliography2++;
        $('.floating_window').addClass('active');
        $('.overlay').addClass('active');
        //var formData = $('#feedback').serializefiles();
        var formData = new FormData(); 
        var contentHtml = "<table>\
        <tr><th style='float:left'><label>Название книги</label></th><th><textarea  style=\"width:250px;resize:vertical\" id='bibliographyNameModal'></textarea></th></tr>\
        <tr><th style='float:left'><label>Автор</label></th><th><textarea  style=\"width:250px;resize:vertical\" id='bibliographyAuthorModal'></textarea></th></tr>\
        <tr><th style='float:left'><label>Город</label></th><th><input type=\"text\" style=\"width:250px;height:22px\" id='bibliographyCityModal'></th></tr>\
        <tr><th style='float:left'><label>Издательство</label></th><th><input type=\"text\" style=\"width:250px;height:22px\" id='bibliographyPublishmentModal'></th></tr>\
        <tr><th style='float:left'><label>Год</label>&nbsp;&nbsp;<input type=\"text\" style=\"width:80px;height:22px\" id='bibliographyYearModal'></th>\
        <th><label>Количество страниц</label>&nbsp;&nbsp;<input type=\"text\" style=\"width:80px;height:22px\" id='bibliographyCountOfListsModal'></th></tr></table>\
        <button style='width:50px' id='okBibliography' onclick='okModal()'>ok</button>";
        $('#content_of_modal_window').html(contentHtml);
    }
   function okModal(){ 
        var div = document.createElement("div");
        var nameOfBook = document.getElementById("bibliographyNameModal").value;
        var author = document.getElementById("bibliographyAuthorModal").value;
        var city = document.getElementById("bibliographyCityModal").value;
        var publishment = document.getElementById("bibliographyPublishmentModal").value;
        var year = document.getElementById("bibliographyYearModal").value;
        var countOfLists = document.getElementById("bibliographyCountOfListsModal").value;
        var indexOfField = 0;
        var bibliographyFlag = false;
        for(let i=0; i<5; i++){
            if(arrayOfBibliography2[i].length == 0){
                bibliographyFlag = true;
                arrayOfBibliography2[i] = [nameOfBook, author, city, publishment, year, countOfLists];
                indexOfField = i+1;
                break;
            }
        }
        if(bibliographyFlag){
		    div.innerHTML = nameOfBook + " , "+ author +" . - "+city+":"+publishment+","+year+"."+countOfLists+" <a onclick=\"return deleteBibliography(this," + indexOfField + ", 'Библиография2')\" href=\"#\">[X]</a>";
		    div.classList.add("simple_text");
		    document.getElementById("biblographyList").appendChild(div);
		}
		else{
		    alert("Число полей достигло своего максимума = " + maxFieldLimit);
		}
	}
	function deleteBibliography(element, index, value){
	    arrayOfBibliography2[index-1] = [];
	    countOfBibliography2--;
	    var contDiv = element.parentNode;
		contDiv.parentNode.removeChild(contDiv);
	}
    (function($) {
    $(document).ready(function() {
        $('.preview').click(function(e) {
           $('.floating_window').addClass('active');
           $('.overlay').addClass('active');
           //var formData = $('#feedback').serializefiles();
           var formData = new FormData(); 
            var contentHtml = "";
            if(e.target.id == 'preview1'){
                var array = $('#disciple :input').serializeArray();
			    $.each(array, function(i, val) {
				    formData.append(val.name, val.value);
			    });
                contentHtml += "<table class='modal_table'>"
                var full_name = "";
                var array = [ 'Имя преподавателя:', 'Дисциплина:', 'Тип:', 'Специальность:', 'Семестры:' , 'Курс:', 'Контроль:']
                var c = 0;
                formData.forEach(function(item, i, formData) {
                    if(i != 'Kafedra' && i != 'fio' && i != 'nam'){
                        if(i == 'fath'){
                            item += " " + full_name;
                        }
                        contentHtml += '<tr><td class="modal_td"><b>'+ array[c] +'</b></td><td class="modal_td">' + item + '</td></tr>';
                        c ++;
                    }
                    else if(i == 'fio' || i == 'nam'){
                        full_name += " " + item;
                    }
                });
                contentHtml += "</table>";
            }
            else if(e.target.id=='preview2'){
                var array = $('.stud :input').serializeArray();
			    $.each(array, function(i, val) {
				    formData.append(val.name, val.value);
			    });
                contentHtml += "<table class='modal_table'>";
                var c = 0;
                contentHtml += "<tr><th>День недели</th><th>Время</th><th>Преподаватель</th><th>Аудитория</th><th>on-line</th></tr>";
                var row = ['', '', '', '', ''];
                formData.forEach(function(item, i, formData) {
                    row[c] = item;
                    c++;
                    if(c == 5){
                        c = 0;
                        var rowContent = "<tr>";
                        var rowFlag = true;
                        for(let l = 0; l<5; l++){
                            if(row[l] != ''){
                                rowContent += "<td>"+row[l]+"</td>";
                            }
                            else{
                                rowFlag = false;
                                break;
                            }
                        }
                        rowContent += "</tr>";
                        if (rowFlag){
                            contentHtml += rowContent;
                        }
                        for(let l = 0; l<5; l++){
                            row[l] = '';
                        }
                    }
                });
                contentHtml += "</table>";
            }
            else if(e.target.id=='preview3'){
                var array = $('.schedule :input').serializeArray();
			    $.each(array, function(i, val) {
				    formData.append(val.name, val.value);
			    });
                contentHtml += "<table class='modal_table'>";
                var c = 0;
                contentHtml += "<tr><th>День недели</th><th>Время</th><th>Преподаватель</th><th>Аудитория</th><th>on-line</th></tr>";
                var row = ['', '', '', '', ''];
                formData.forEach(function(item, i, formData) {
                    row[c] = item;
                    c++;
                    if(c == 5){
                        c = 0;
                        var rowContent = "<tr>";
                        var rowFlag = true;
                        for(let l = 0; l<5; l++){
                            if(row[l] != ''){
                                rowContent += "<td>"+row[l]+"</td>";
                            }
                            else{
                                rowFlag = false;
                                break;
                            }
                        }
                        rowContent += "</tr>";
                        if (rowFlag){
                            contentHtml += rowContent;
                        }
                        for(let l = 0; l<5; l++){
                            row[l] = '';
                        }
                    }
                });
                contentHtml += "</table>";
            }
            else if(e.target.id=='preview4'){
                var array = $('#bibliography :input').serializeArray();
			    $.each(array, function(i, val) {
				    formData.append(val.name, val.value);
			    });
                contentHtml += "<table class='modal_table'>";
                var c = 0;
                contentHtml += "<tr><th>Название книги:</th><th>Автор</th><th>Город</th><th>Издательство</th><th>Год</th><th>Кол-во страниц</th></tr>";
                var row = ['', '', '', '', '', ''];
                formData.forEach(function(item, i, formData) {
                    row[c] = item;
                    c++;
                    if(c == 6){
                        c = 0;
                        var rowContent = "<tr>";
                        var rowFlag = true;
                        for(let l = 0; l<6; l++){
                            if(row[l] != ''){
                                rowContent += "<td>"+row[l]+"</td>";
                            }
                            else{
                                rowFlag = false;
                                break;
                            }
                        }
                        rowContent += "</tr>";
                        if (rowFlag){
                            contentHtml += rowContent;
                        }
                        for(let l = 0; l<6; l++){
                            row[l] = '';
                        }
                    }
                });
                contentHtml += "</table>";
            }
            else if(e.target.id=='preview5'){
                var array = $('#Links :input').serializeArray();
			    $.each(array, function(i, val) {
				    formData.append(val.name, val.value);
			    });
                var c = 0;
                var row = ['', ''];
                contentHtml += "<ul>"
                formData.forEach(function(item, i, formData) {
                    row[c] = item;
                    c++;
                    if(c == 2){
                        c = 0;
                        var rowContent = "";
                        var rowFlag = true;
                        if(row[0] != '' && row[1] != ''){
                            rowContent = "<li><a href='"+row[1]+"'>"+row[0]+"</a></li>";
                        }
                        else{
                            rowFlag = false;
                        }
                        if (rowFlag){
                            contentHtml += rowContent;
                        }
                        for(let l = 0; l<2; l++){
                            row[l] = '';
                        }
                    }
                });
                contentHtml += "</ul>";
            }
            else if(e.target.id=='preview6'){
                 var array = $('#ElectronicDocuments :input').serializeArray();
			    $.each(array, function(i, val) {
				    formData.append(val.name, val.value);
			    });
                var c = 0;
                var row = [''];
                contentHtml += "<ul>";
                formData.forEach(function(item, i, formData) {
                    row[c] = item;
                    c++;
                    if(c == 1){
                        c = 0;
                        var rowContent = "";
                        var rowFlag = true;
                        if(row[0] != ''){
                            rowContent = "<li><a href='https://vk.com'>"+row[0]+"</a></li>";
                        }
                        else{
                            rowFlag = false;
                        }
                        if (rowFlag){
                            contentHtml += rowContent;
                        }
                        for(let l = 0; l<1; l++){
                            row[l] = '';
                        }
                    }
                });
                contentHtml += "</ul>";
            }
            else if(e.target.id=='preview10'){
                contentHtml += "<ul>";
                for(let i = 0; i<5; i++){
                    if (arrayOfBibliography2[i] != ""){
                        contentHtml += "<li>"+arrayOfBibliography2[i][0] + " , "+ arrayOfBibliography2[i][1] +" . - "+arrayOfBibliography2[i][2]+":"+arrayOfBibliography2[i][3]+","+arrayOfBibliography2[i][4]+"."+arrayOfBibliography2[i][5]+"</li>";
                    }
                }
                contentHtml += "</ul>";
            }
            $('#content_of_modal_window').html(contentHtml);
        });
    });
    }(jQuery));
     (function($) {
    $(document).ready(function() {
        $('.modal__cross').click(function(e) {
           $('.floating_window').removeClass('active');
           $('.overlay').removeClass('active');
           $('#content_of_modal_window').html("");
        });
    });
    }(jQuery));
    function openBlock(evt, blockName) {
            var i, tabcontent, tablinks, button;
           tabcontent = document.getElementsByClassName("block");
           button = document.getElementById("Create");
           button.style.display = "none";
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(blockName).style.display = "block";
            evt.currentTarget.className += " active";
            if (blockName == 'electronic_resources' ){
                button.style.display = "block";
            }
    }
	function deleteField1(a, index, category) {
		// Получаем доступ к ДИВу, содержащему поле
		var contDiv = a.parentNode;
		// Удаляем этот ДИВ из DOM-дерева
		contDiv.parentNode.removeChild(contDiv);
		// Уменьшаем значение текущего числа полей
		if (category == "Ссылка") {
			countOfLinks--;
			arrayOfLinks[index] = 0;
			
		} else if (category == "Предмет") {
			countOfSubs--;
			arrayOfSubs[index] = 0;
		} else if (category == "Файл") {
			countOfBooks--;
			arrayOfBooks[index] = 0;
		} else if (category == "Консультация") {
			countOfFields--;
			arrayOfHD[index] = 0;
		} else if (category == "Библиография"){
		    countOfBibliography--;
		    arrayOfBibliography[index] = 0;
		}
		// Возвращаем false, чтобы не было перехода по сслыке
		return false;
	}
	
	function deleteRowOfTable(a, index, name){
	    if(name == "Ссылка"){
	        var T = document.getElementById ('parentLink');
	        ell = a.parentNode.parentNode; // tr element (ваша строчка)
            ell.parentNode.removeChild(ell);
	        countOfLinks--;
		    arrayOfLinks[index] = 0;
	    }
	    else if(name == "Консультация"){
	        var T = document.getElementById ('parentHD');
	        ell = a.parentNode.parentNode; // tr element (ваша строчка)
            ell.parentNode.removeChild(ell);
	        countOfFields--;
		    arrayOfHD[index] = 0;
	    }
	     else if(name == "Расписание"){
	        var T = document.getElementById ('parentSub');
	        ell = a.parentNode.parentNode; // tr element (ваша строчка)
            ell.parentNode.removeChild(ell);
	        countOfSubs--;
		    arrayOfSubs[index] = 0;
	    }
	    else if(name == "Библиография"){
	        var T = document.getElementById ('parentBibliography');
	        ell = a.parentNode.parentNode; // tr element (ваша строчка)
            ell.parentNode.removeChild(ell);
	        countOfBibliography--;
		    arrayOfBibliography[index] = 0;
	    }
	    else if(name == "Файл"){
	        var T = document.getElementById ('parentBook');
	        ell = a.parentNode.parentNode; // tr element (ваша строчка)
            ell.parentNode.removeChild(ell);
	        countOfBooks--;
		    arrayOfBooks[index] = 0;
	    }
		return false;
	}
	function addField(id) {
		// Увеличиваем текущее значение числа полей
		if (id == "parentHD") {
			// Проверяем, не достигло ли число полей максимума
			if (countOfFields >= maxFieldLimit) {
				alert("Число полей достигло своего максимума = " + maxFieldLimit);
				return false;
			}
			countOfFields++;
			// Увеличиваем ID
			curFieldNameId++;
			var index3 = 1;
			for (let i = 1; i < 5; i++) {
				if (arrayOfHD[i] == 0) {
					arrayOfHD[i] = 1;
					index3 = i;
					break;
				}
			}
			var num3 = index3 + 1;
			var tbody = document.getElementById("parentHD").getElementsByTagName('tbody')[0];
			var row = document.createElement("tr");
			tbody.appendChild(row);
			var th1 = document.createElement("TH");
            var th2 = document.createElement("TH");
            var th3 = document.createElement("TH");
            var th4 = document.createElement("TH");
            var th5 = document.createElement("TH");
            var th6 = document.createElement("TH");
            row.appendChild(th1);
            row.appendChild(th2);
            row.appendChild(th3);
            row.appendChild(th4);
            row.appendChild(th5);
            row.appendChild(th6);
            th1.innerHTML = "<select style=\"width:150px;height:30px; font-size:15px\" name=\"den_" + num3 + "\"><option value=\"Понедельник\">Понедельник</option><option value=\"Вторник\">Вторник</option><option value=\"Четверг\">Четверг</option><option value=\"Пятница\">Пятница</option><option value=\"Суббота\">Суббота</option></select>";
            th2.innerHTML ="<input style=\"height:30px; font-size:15px\" name=\"timee_" + num3 + "\" type=\"time\" />";
            th3.innerHTML = "<input style=\"width:150px;height:22px; font-size:15px\" name=\"teacher_" + num3 + "\" type=\"text\" />";
            th4.innerHTML = "<input style=\"width:60px;height:22px\" name=\"class_" + num3 + "\" type=\"number\" />";
            th5.innerHTML = "<input style=\"width:150px;height:22px; font-size:15px\" name=\"online_" + num3 + "\" type=\"text\" />";
            th6.innerHTML = "<a onclick=\"return deleteRowOfTable(this," + index3 + ", 'Консультация')\" href=\"#\">[X]</a>";
		} else if (id == "parentLink") {
			if (countOfLinks >= maxFieldLimit) {
				alert("Число полей достигло своего максимума = " + maxFieldLimit);
				return false;
			}
			curLinkNameId++;
			countOfLinks++;
			var index = 1;
			for (let i = 1; i < 5; i++) {
				if (arrayOfLinks[i] == 0) {
					arrayOfLinks[i] = 1;
					index = i;
					break;
				}
			}
			var num = index + 1;
			var tbody = document.getElementById("parentLink").getElementsByTagName('tbody')[0];
			var row = document.createElement("tr");
			tbody.appendChild(row);
			var th1 = document.createElement("TH");
            var th2 = document.createElement("TH");
            var th3 = document.createElement("TH");
            var th4 = document.createElement("TH");
            row.appendChild(th1);
            row.appendChild(th2);
            row.appendChild(th3);
            row.appendChild(th4);
            //th1.innerHTML = "<input style=\"width:250px\" type=\"text\" required name=\"hyperName_" + num + "\">";
            th1.innerHTML = "<textarea style=\"width:250px;resize:vertical\"  name=\"hyperName_"+num+ "\"></textarea>";
            th2.innerHTML ="<input style=\"width:400px\" required  name=\"hyper_" + num + "\" type=\"text\" />";
            th3.innerHTML = "<a onclick=\"return deleteRowOfTable(this," + index + ", 'Ссылка')\" href=\"#\">[X]</a>";
            th4.innerHTML = "<label id=\"hyper_" + num + "_error\" class=\"error\"></label>";
            th4.setAttribute('align', 'center');
            th4.setAttribute('valign', 'top');
			
		} else if (id == "parentSub") {
			if (countOfSubs >= maxFieldLimit) {
				alert("Число полей достигло своего максимума = " + maxFieldLimit);
				return false;
			}
			curSubNameId++;
			countOfSubs++;
			var index2 = 1;
			for (let i = 1; i < 5; i++) {
				if (arrayOfSubs[i] == 0) {
					arrayOfSubs[i] = 1;
					index2 = i;
					break;
				}
			}
			var num2 = index2 + 1;
			var tbody = document.getElementById("parentSub").getElementsByTagName('tbody')[0];
			var row = document.createElement("tr");
			tbody.appendChild(row);
			var th1 = document.createElement("TH");
            var th2 = document.createElement("TH");
            var th3 = document.createElement("TH");
            var th4 = document.createElement("TH");
            var th5 = document.createElement("TH");
            var th6 = document.createElement("TH");
            row.appendChild(th1);
            row.appendChild(th2);
            row.appendChild(th3);
            row.appendChild(th4);
            row.appendChild(th5);
            row.appendChild(th6);
            th1.innerHTML = "<select style=\"width:150px;height:30px; font-size:15px\" name=\"DenOfSub_" + num2 + "\"><option value=\"Понедельник\" >Понедельник</option><option value=\"Вторник\">Вторник</option><option value=\"Четверг\">Четверг</option><option value=\"Пятница\">Пятница</option><option value=\"Суббота\">Суббота</option></select>";
            th2.innerHTML ="<input  style=\"height:30px; font-size:15px\" name=\"timeOfSub_" + num2 + "\" type=\"time\" />";
            th3.innerHTML = "<input style=\"width:150px;height:22px; font-size:15px\" name=\"teacherOfSub_" + num2 + "\" type=\"text\" />";
            th4.innerHTML = "<input style=\"width:60px;;height:22px\" name=\"classOfSub_" + num2 + "\" type=\"number\" />";
            th5.innerHTML = "<input style=\"width:150px;height:22px; font-size:15px\" name=\"onlineOfSub_" + num2 + "\" type=\"text\" />";
            th6.innerHTML = "<a onclick=\"return deleteRowOfTable(this," + index2 + ", 'Расписание')\" href=\"#\">[X]</a>";
		} else if (id == "parentBook") {
			if (countOfBooks >= maxFieldLimit) {
				alert("Число полей достигло своего максимума = " + maxFieldLimit);
				return false;
			}
			curBookNameId++;
			countOfBooks++;
			var index4 = 1;
			for (let i = 1; i < 5; i++) {
				if (arrayOfBooks[i] == 0) {
					arrayOfBooks[i] = 1;
					index4 = i;
					break;
				}
			}
			var num4 = index4 + 1;
			/*var div = document.createElement("div");
			div.innerHTML = "<br /><label>Имя файла: </label><input style=\"width:150px\" type=\"text\" required name=\"book_" + num4 + "\"><input type=\"file\" required name=\"filename_" + num4 + "\" ><a onclick=\"return deleteField1(this," + index4 + ",'Файл')\" href=\"#\">[X]</a><label id='filename_" + num4 + "_error' class=\"error\"></label><br /><br />";*/
			var tbody = document.getElementById("parentBook").getElementsByTagName('tbody')[0];
			var row = document.createElement("tr");
			tbody.appendChild(row);
			var th1 = document.createElement("TH");
            var th2 = document.createElement("TH");
            var th3 = document.createElement("TH");
            var th4 = document.createElement("TH");
            row.appendChild(th1);
            row.appendChild(th2);
            row.appendChild(th3);
            row.appendChild(th4);
            th1.innerHTML="<textarea style=\"width:250px;resize:vertical\" required  name=\"book_" + num4 + "\">";
            th2.innerHTML ="<input type=\"file\" required name=\"filename_" + num4 + "\" >";
            th3.innerHTML="<a onclick=\"return deleteRowOfTable(this," + index4 + ", 'Файл')\" href=\"#\">[X]</a>"
            th4.innerHTML ="<label id=\"filename_" + num4 + "_error\" class=\"error\"></label>";
		}    else if (id == "parentBibliography") {
			if (countOfBibliography >= maxFieldLimit) {
				alert("Число полей достигло своего максимума = " + maxFieldLimit);
				return false;
			}
			curBibliographyName++;
			countOfBibliography++;
			var index5 = 1;
			for (let i = 1; i < 5; i++) {
				if (arrayOfBooks[i] == 0) {
					arrayOfBooks[i] = 1;
					index5 = i;
					break;
				}
			}
			var num5 = index5 + 1;
			var tbody = document.getElementById("parentBibliography").getElementsByTagName('tbody')[0];
			var row = document.createElement("tr");
			tbody.appendChild(row);
			var th1 = document.createElement("TH");
            var th2 = document.createElement("TH");
            var th3 = document.createElement("TH");
            var th4 = document.createElement("TH");
            var th5 = document.createElement("TH");
            var th6 = document.createElement("TH");
            var th7 = document.createElement("TH");
            var th8 = document.createElement("TH");
            row.appendChild(th1);
            row.appendChild(th2);
            row.appendChild(th3);
            row.appendChild(th4);
            row.appendChild(th5);
            row.appendChild(th6);
            row.appendChild(th7);
            row.appendChild(th8);
            th1.innerHTML = "<input style=\"width:150px\" required type=\"text\" name=\"bibliography_name_" + num5 + "\" >";
            th2.innerHTML ="<input style=\"width:150px\" type=\"text\" name = \"bibliography_author_"+num5+"\">";
            th3.innerHTML = "<input style=\"width:150px\" name=\"bibliography_city_"+num5+"\" type=\"text\">";
            th4.innerHTML ="<input type=\"text\" style=\"width:150px\" name=\"bibliography_publishing_"+num5+"\">";
            th5.innerHTML = "<input type=\"text\" style=\"width:150px\" name=\"bibliography_year_"+num5+"\">";
            th6.innerHTML ="<input type=\"text\" style=\"width:150px\" name=\"bibliography_number_of_pages_"+num5+"\">";
            th7.innerHTML = "<a onclick=\"return deleteRowOfTable(this," + index5 + ", 'Библиография')\" href=\"#\">[X]</a>";
            th8.innerHTML = "<label id=\"bibliography_"+num5+"_error\" class=\"error\"></label>";
            th8.setAttribute('align', 'center');
            th8.setAttribute('valign', 'top');
		} else if (id == "parentPlan") {
		    if (countOfPlans >= maxFieldLimit) {
				alert("Число полей достигло своего максимума = " + maxFieldLimit);
				return false;
			}
			curPlanNameId++;
			countOfPlans++;
			var index6 = 1;
			for (let i = 1; i < 5; i++) {
				if (arrayOfPlans[i] == 0) {
					arrayOfPlans[i] = 1;
					index6 = i;
					break;
				}
			}
			var num6 = index6 + 1;
			var tbody = document.getElementById("parentPlan").getElementsByTagName('tbody')[0];
			var row = document.createElement("tr");
			tbody.appendChild(row);
			for (let i=0;i<arrayOfColumnNamesOfPlan.length; i++){
			    let th = document.createElement("TH");
			    if(arrayOfColumnTypesOfPlan[i] == "inputText"){
			        th.innerHTML = "<input style=\"height:22px; font-size:15px\" required type=\"text\" name='"+arrayOfColumnNamesOfPlan[i]+"_" + num6 + "' >";
			    }
			    else if(arrayOfColumnTypesOfPlan[i] == "inputNumber"){
			        th.innerHTML = "<input style=\"height:22px; font-size:15px;width:150px\" required type=\"number\" name='"+arrayOfColumnNamesOfPlan[i]+"_" + num6 + "' >";
			    }
			    else if(arrayOfColumnTypesOfPlan[i] == "inputFile"){
			        th.innerHTML = "<input style=\" font-size:15px\" required type=\"file\" name='"+arrayOfColumnNamesOfPlan[i]+"_" + num6 + "' >";
			    }
			    row.appendChild(th.cloneNode(true));
			}
			var th1 = document.createElement("TH");
			row.appendChild(th1);
			th1.innerHTML = "<a onclick=\"return deleteRowOfPlan(this," + index6 + ", '')\" href=\"#\">[X]</a>";
	    }
		// Возвращаем false, чтобы не было перехода по сслыке
		return false;
	}
	$(document).ready(function() {
		$('.block').on('click', '.event-click', function() {
			$(this).siblings('.stud').slideToggle(0);
		});
	});
	$(document).ready(function() {
		$('.block').on('click', '.event-click2', function() {
			$(this).siblings('.schedule').slideToggle(0);
		});
	});
	$('#send').click(function() {
		var formData = $('#feedback').serializefiles();
		$('.error').hide();
		$('input').each(function() {
			$(this).removeClass('error_input');
		});
		$.ajax({
			type: 'POST',
			url: 'https://playing-precaution.000webhostapp.com/ConstructorHandler.php',
			data: formData,
			async: false,
			dataType: "json",
			cache: false,
			contentType: false,
			processData: false,
			success: function(data) {
				if (data.result == 'error') {
					alert("Не удалось создать сайт");
					for (var errorField in data.text_error) {
						// выводим текст ошибок 
						$('#' + errorField + '_error').html(data.text_error[errorField]);
						// показываем текст ошибок
						$('#' + errorField + '_error').show();
						// обводим инпуты красным цветом
						//$('#'+errorField).addClass('error_input');                      
					}
				} else if (data.result == 'success') {
					alert("Сайт был успешно создан");
				}
			}
		});
	});
	(function($) {
		$.fn.serializefiles = function() {
			var obj = $(this);
			/* ADD FILE TO PARAM AJAX */
			var formData = new FormData();
			$.each($(obj).find("input[type='file']"), function(i, tag) {
				$.each($(tag)[0].files, function(i, file) {
					formData.append(tag.name, file);
				});
			});
			var params = $(obj).serializeArray();
			$.each(params, function(i, val) {
				formData.append(val.name, val.value);
			});
			alert(arrayOfBibliography2);
			for(let i = 0; i<5;i++){
			    if(arrayOfBibliography2[i].length != 0){
			        var index_b = i+1;
			        formData.append("bibliography2_"+index_b, arrayOfBibliography2[i][0] + " , "+ arrayOfBibliography2[i][1] +" . - "+arrayOfBibliography2[i][2]+":"+arrayOfBibliography2[i][3]+","+arrayOfBibliography2[i][4]+"."+arrayOfBibliography2[i][5]);
			    }
			}
			return formData;
		};
	})(jQuery);
</script>