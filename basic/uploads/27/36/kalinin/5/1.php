<?php
session_start();
if (empty($_SESSION["login"])) { 
	echo "<script>document.location.replace('index.php');</script>";
	exit();
}
$usr = $_SESSION["login"];
require_once "PhpLibrary.php";
$No_Load = "";
$No_Site = "";
if (isset($_POST["send1"])) { 
	echo "<script>document.location.replace('Constructor.php');</script>";
	exit();
}
if (isset($_POST["send2"])) {
	if (file_exists("" . $usr . "/site.html")) {
		header('Location:' . $usr . '/site.html');
		exit();
	} else {
		$No_Site = "Вы ещё не создали сайт";
	}
}
if (isset($_POST["send3"])) {
	if (file_exists('' . $usr . '/site.html')) {
		zip("./" . $usr, "./compressed.zip");
		file_force_download('compressed.zip');
		unlink("compressed.zip");
		exit();
	} else {
		$No_Load = "Вы ещё не создали сайт";
	}
}
if (isset($_POST["send4"])) {
	session_destroy();
	header('Location:index.php');
	exit();
}
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<title> Работа с конструктором</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
	<div id="content">
	<header id="headblock">
		<h1 class="head">Работа с конструктором сайтов</h1>
	</header><br />
	<div id="exit">
		<form name="exit"  method="post">
			<input id="butt" type="submit" style="width:80px" name="send4" value="выйти" />
			<label>Пользователь: <?php echo $_SESSION["login"] ?> </label>
		</form>
	</div>
	<div id="distributor">
		<form name="feedback"  method="post">
			<br />
			<label class="lab"><span class="imag"></span> &nbsp;Создайте сайт дисциплины с нуля</label><br />
			<input type="submit" class="inp" style="width:225px" name="send1" value="Перейти к конструктору сайтов" /><br />
			<br />
			<label class="lab"><span class="imag2"></span> &nbsp;Перейти к вашему сайту</label>
			<span class="head" style="color: red; font-size:15px"><?= $No_Site ?></span><br /><input type="submit" class="inp" style="width:225px" name="send2" value="Ваш сайт" /><br /><br />
			<label class="lab"><span class="imag3"></span> &nbsp;Выгрузите ваш сайт</label>
			<span class="head" style="color: red; font-size:15px"><?= $No_Load ?></span><br /><input type="submit" class="inp" style="width:225px" name="send3" value="выгрузить сайт" />
		</form>
	</div><br />
	</div>
	<footer id="footerBlock"><a class = "information_of_developer">Разработчик: Никита Калинин</a></footer>
</body>

</html>