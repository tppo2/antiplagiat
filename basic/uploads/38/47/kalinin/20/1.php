<?php
$flg = false;
$error_login = "";
$error_pass = "";
$error = false;
$proxy = "";
if (isset($_POST["send"])) {
	$login = htmlspecialchars($_POST["login"]);
	$pass = htmlspecialchars($_POST["pass"]);
	if ($login == "") {
		$error_login = "Введите корректный логин";
		$error = true;
	}
	if ($pass == "") {
		$error_pass = "Введите корректный пароль";
		$error = true;
	}
	if (!$error) {
		$mysqli = new mysqli("localhost", "id11089137_kalinin", "Ktnj5414", "id11089137_mybase");
		$mysqli->query("SET NAMES	'utf8'");
		$result_set = $mysqli->query("SELECT * FROM `teachers`");
		while (($row = $result_set->fetch_assoc()) != false) {
			$ps = md5($pass);
			$ps1 = $row["password"];
			if ($row["login"] == $login && $ps1 == $ps) {
				$flg = true;
				$proxy = $row["proxy"];
				break;
			}
		}
		if ($flg == false) {
			$error_login = "Вы ввели неправильный логин/пароль";
			$error_pass = "Вы ввели неправильный логин/пароль";
		} else if ($flg == true) {
			session_start();
			$_SESSION["login"] = $login;
			$_SESSION["proxy"] = $proxy;
			if ($proxy == "user") {
				header('Location:Distributor.php');
			} else if ($proxy == "admin") {
				header('Location:forAdmin.php');
			}
			exit();
		}
	}
}
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<!--Подключаем стили из другого файла-->
	<title> Конструктор сайтов</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Конструктор сайтов для преподавателей ПетрГу" />
</head>

<body>
	<div id="content">
		<header id="headblock">
			<h1 class="head">Конструктор сайтов учебной дисциплины </h1>
		</header><br />
		<div id="artic"><small class="mycl">Этот конструктор был создан чтобы помочь преподавателям, которые не владеют языками программирования, создать сайт дисциплины.</small></div>
		<div id="asid">
			<h2>Авторизация</h2>
			<form name="feedback" method="post">
				<input class="IndexPass" type="text" name="login" placeholder="Введите логин" style="font-size:0.8em"/>
				<span class="head" style="color: red"><?= $error_login ?></span>
				<br />
				<br />
				<input class="IndexPass" type="password" name="pass" placeholder="Введите пароль" style="font-size:0.8em"/>
				<span class="head" style="color: red"><?= $error_pass ?></span>
				<br />
				<br />
				<input class="head" type="submit" name="send" value="Отправить" />
			</form>
		</div>
	</div>
	<footer id="footerBlock"><a class="information_of_developer">Разработчик: Никита Калинин</a></footer>
</body>

</html>