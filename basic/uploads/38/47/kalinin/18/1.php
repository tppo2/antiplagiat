<?php
session_start();
if (empty($_SESSION["login"])) {
    echo "<script>document.location.replace('index.php');</script>";
    exit();
}
$usr = $_SESSION["login"];
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <title>Ваши сайты</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
    <div id="content">
        <div id="headblock">
            <h1 class="head">Ваши сайты</h1>
        </div><br />
        <div id="exit">
            <form name="exit" method="post">
                <input id="butt" type="submit" style="width:80px" name="send4" value="выйти" />
                <label>Пользователь: <?php echo $_SESSION["login"] ?> </label>
                <input id="backButton" type="submit" name="BackSubmit" style="width:80px" value="назад" />
            </form>
        </div>
        <a href='./'.$usr.'/site.html'> сайт</a>
    </div>
    <div id="footerBlock"><a class="information_of_developer">Разработчик: Никита Калинин</a></div>
</body>

</html>