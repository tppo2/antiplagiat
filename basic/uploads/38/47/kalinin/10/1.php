<?php
require_once "PhpLibrary.php";
session_start();
if (empty($_SESSION["login"]) || $_SESSION["proxy"] != "admin") {
	header('Location:index.php');
	exit();
}
$mysqli = new mysqli("localhost", "id11089137_kalinin", "Ktnj5414", "id11089137_mybase");
$mysqli->query("SET NAMES	'utf8'");
if (isset($_POST["send4"])) {
	session_destroy();
	echo "<script>document.location.replace('index.php');</script>";
	exit();
}
if (isset($_POST["add"])) {
	$login = htmlspecialchars($_POST["login"]);
	$pass = htmlspecialchars($_POST["pass"]);
	$proxy = htmlspecialchars($_POST["proxy"]);
	if ($login != "" && $pass != "" && ($proxy == "admin" || $proxy == "user")) {
		$res = $mysqli->query("INSERT INTO `teachers` (`login`,`password`,`proxy`) VALUES('" . $login . "', '" . md5("" . $pass . "") . "', '" . $proxy . "')");
		if (!file_exists("./" . $login) && $res != FALSE)
			mkdir("./" . $login);
	}
}
if (isset($_POST["delete"])) {
	$minus = htmlspecialchars($_POST["minus"]);
	$idd = htmlspecialchars($_POST["idd"]);
	if ($minus != "" && $idd != "") {
		$mysqli->query("DELETE FROM `teachers` WHERE `teachers`.`id`=" . $idd . "");
		if (file_exists("./" . $minus)) {
			recursiveRemoveDir("./" . $minus);
		}
	}
}
?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<title> Управление учётными записями</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body>
	<div id="content">
		<div id="headblock">
			<h1 class="head">Управление учётными записями</h1>
		</div>
		<div id="exit">
			<form name="exit" method="post">
				<input id="butt" type="submit" style="width:80px" name="send4" value="выйти" />
				<label>Пользователь: <?php echo $_SESSION["login"] ?> </label>
			</form>
		</div>
		<div class="SecondHalf">
			<h2>Список пользователей</h2>
			<div id="ListOfUsers">
				<table id="tableOfUsers">
					<tr>
						<th>id</th>
						<th>Логин</th>
						<th>Статус</th>
					</tr>
					<?php
					$result_set = $mysqli->query("SELECT * FROM `teachers`");
					while (($row = $result_set->fetch_assoc()) != false) {
						echo "<tr><td>" . $row["id"] . "</td><td>" . $row["login"] . "</td><td>" . $row["proxy"] . "</td></tr>";
						echo "<br />";
					}
					?>
				</table><br /><br />
			</div>
		</div>
		<div style="width:400px;float:right; margin-right:18%;">
			<div class="FirstHalf">
				<h2>Добавить пользователя</h2>
				<div id="adU"><br />
					<form name="feedback" method="post">
						<label class="labelAd"> Введите login </label><br /><input class="Inp" type="text" name="login" />
						<br />
						<label class="labelAd"> Введите пароль: </label><br /><input class="Inp" type="password" name="pass" />
						<br />
						<label class="labelAd"> Введите статус: </label><br /><input class="Inp" type="text" name="proxy" />
						<br />
						<input type="submit" class="Add-delete" name="add" value="добавить" />
					</form><br /><br />
				</div>
			</div>
			<br />
			<div class="FirstHalf">
				<h2>Удалить пользователя</h2>
				<div id="delU"><br />
					<form name="feedbackkk" method="post">
						<label class="labelAd"> Введите id </label><br /><input class="Inp" type="text" name="idd" />
						<br />
						<label class="labelAd"> Введите логин </label><br /><input class="Inp" type="text" name="minus" /><br />
						<input type="submit" class="Add-delete" name="delete" value="удалить" />
					</form><br /><br />
				</div>
			</div>
		</div>
	</div>
	<div id="footerBlock"><a class="information_of_developer">Разработчик: Никита Калинин</a></div>
</body>