<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\AuthForm;
use app\models\EditTaskForm;
use app\models\EditTaskItemForm;
use app\models\TblUser;
use app\models\Task;
use app\models\TaskItem;
use app\models\Work;
use app\models\WorkItem;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use app\models\Course;
use app\models\UploadFile;
use yii\web\UploadedFile;

class TaskController extends Controller
{
	public function actionTask($idtask=null){
		if (isset($_SESSION['auth'])){
			if(!isset($idtask)){
                		return $this->redirect(["auth/mainpage"]);
            		}
			$usr = TblUser::find()->where(['login'=>$_SESSION['auth']])->one();

			$task = Task::findOne($idtask);
			
			$taskItems_exist = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->exists();
			$taskItems = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->all();
			$taskItemsCount = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->count();

			$uf = new UploadFile();
			$max_vers = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'isDeleted'=>0])->max('Version');
			
			$work_exist = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'isDeleted'=>0, 'Version'=>$max_vers])->exists();
			$work = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'isDeleted'=>0, 'Version'=>$max_vers])->one();
			if ($work_exist){
				$workItems_exist = WorkItem::find()->where(['idWork'=>$work['idWork'], 'isDeleted'=>0])->exists();
            			$workItems = WorkItem::find()->where(['idWork'=>$work['idWork'], 'isDeleted'=>0])->all();
			}
			else{
				$workItems_exist = false;
				$workItems = null;
			}
            
			if(Yii::$app->request->post('delete_task_task_'.$task['idTask'])){
                		$task -> isDeleted = 1;
                		$task -> save();
                		return $this->redirect(['course/course', 'id'=>$task->idCourse]);
            		}
            		if(Yii::$app->request->post('edit_task_task_'.$task['idTask'])){
                		return $this->redirect(array('edittask','idtask'=>$idtask));
            		}
			if(Yii::$app->request->post('upload_taskItems'.$task['idTask']))
			{
				$vers = 0;
				$work_exist = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'isDeleted'=>0, 'Version'=>$max_vers])->exists();
				if ($work_exist)
				{
					$vers = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'isDeleted'=>0])->max('version');
	    			}
				if (($task['tries'] == 0 || $task['tries'] > $vers) && $uf->validate())
				{
					$new_vers = $vers + 1;
					if (!file_exists('uploads/' . $task->idCourse . '/' . $task->idTask . '/' . $_SESSION['auth']))
					{
						mkdir('uploads/' . $task->idCourse . '/' . $task->idTask . '/' . $_SESSION['auth']);
					}
					if (!file_exists('uploads/' . $task->idCourse . '/' . $task->idTask . '/' . $_SESSION['auth'] . '/' . $new_vers))
					{
						mkdir('uploads/' . $task->idCourse . '/' . $task->idTask . '/' . $_SESSION['auth']. '/' . $new_vers);
					}

					$w = new Work();
					$w->idTask = $task['idTask'];
					$w->idUser = $usr['idUser'];
					$w->date = date('Y-m-d');
					$w->uniqueness = -1;
					$w->version = $new_vers;
					$w->isDeleted = 0;
					$w->save();

					$w1 = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'isDeleted'=>0, 'Version'=>$new_vers])->one();
					
	    				for ($i = 0; $i < $taskItemsCount; $i++)
					{
						$ti = $taskItems[$i];
						$file = UploadedFile::getInstance($uf, 'file1['.$i.']');
						if ($file == null)
						{
							$w1->isDeleted = 1;
							$w1->save();
						}
						else
						{
							$filename = $uf->upload($i, $task['idCourse'], $task['idTask'], $_SESSION['auth'], $new_vers, $ti['num'], $file);
	    						if($filename != false)
							{
								$wi = new WorkItem();
								$wi->idWork = $w1['idWork'];
								$wi->num = $ti['num'];
								$wi->file = $filename;
								$wi->uniqueness = -1;
								$wi->isDeleted = 0;
								$wi->save();
	    						}
							else
							{
								$w1->isDeleted = 1;
								$w1->save();

							}
						}
					}
					return $this->refresh();
				}
			}
			if($workItems_exist){
				foreach($workItems as &$item)
				{
					if(Yii::$app->request->post('download_file_'.$item['idWorkItem']))
					{
						$w = Work::findOne($item['idWork']);
						$path = __DIR__ . '/../web/uploads/' . $task['idCourse'] . '/' . $task['idTask'] . '/' . $_SESSION['auth'] . '/' . $w['version'] . '/';
						$file = $path . $item['file'];

						if (file_exists($file)) 
						{
							return \Yii::$app->response->sendFile($file);
						} 
						throw new \Exception('File not found');
					}
				}
			}


	        if(Yii::$app->request->post('open_report_task')){
                return $this->redirect(array('report/reporttask','idTask'=>$task['idTask']));
            }

			return $this->render('task', ['task'=>$task, 'taskItems_exist'=>$taskItems_exist, 'taskItems'=>$taskItems, 'work_exist'=>$work_exist, 'workItems'=>$workItems, 'workItems_exist'=>$workItems_exist, 'uf'=>$uf]);
		}
	}
	public function actionEdittask($idtask=null){
		if (isset($_SESSION['auth'])){
			$task = Task::findOne($idtask);
			$form = new EditTaskForm();
			$taskItems_exist = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->exists();
            		$taskItems = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->all();

			//$usr = TblUser::find()->where(['login'=>$_SESSION['auth']])->one();

			//$max_vers = Work::find()->where(['idTask'=>$task['idTask']])->andWhere(['idUser'=>$usr['idUser']])->max('Version');
			
			//$work_exist = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'Version'=>$max_vers])->exists();
			//$work = Work::find()->where(['idTask'=>$task['idTask'], 'idUser'=>$usr['idUser'], 'Version'=>$max_vers])->one();
			//if ($work_exist){
			//	$workItems_exist = WorkItem::find()->where(['idWork'=>$work['idWork']])->exists();
            		//	$workItems = WorkItem::find()->where(['idWork'=>$work['idWork']])->all();
			//}
			//else{
			//	$workItems_exist = false;
			//	$workItems = null;
			//}
			if(Yii::$app->request->post('add_item_'.$task['idTask'])){
				$fnum = TaskItem::find()->where(['idTask'=>$task['idTask'], 'isDeleted'=>0])->max('num');
                		$item = new TaskItem();
                		$item->idTask = $task['idTask'];
                		$item->num = $fnum+1;
                		$item->comment = "123";
                		$item->isDeleted = 0;
                		if($item->save()){
					$task['filesQuantity'] = $task['filesQuantity']+1;
					$task->save();
				}

				if (!file_exists('uploads/' . $task->idCourse . '/' . $task->idTask . '/teacher_files'))
				{
					mkdir('uploads/' . $task->idCourse . '/' . $task->idTask . '/teacher_files');
				}


                		return $this->refresh();
            		}


			if($form->load(Yii::$app->request->post()) && Yii::$app->request->post('save_task_edittask_'.$task['idTask']) && $form->validate()){
                		$task->task = Html::encode($form->task_name);
                		$task->comment = Html::encode($form->task_comment);
                		$task->tries = Html::encode($form->task_tries);
                		$task->year = Html::encode($form->task_years);
                		$task->uniqueness = Html::encode($form->task_uniqueness);
                		$task->save();
                		return $this->redirect(['task', 'idtask'=>$idtask]);
            		}
			if(Yii::$app->request->post('delete_task_edittask_'.$task['idTask'])){
                		$task->isDeleted = 1;
                		$task->save();
                		return $this->redirect(['course/course', 'id'=>$task->idCourse]);
            		}
			foreach($taskItems as &$item){
                		if(Yii::$app->request->post('open_item_edittask_'.$item->idTaskItem)){
                    			return $this->redirect(['edittaskitem','idtaskitem'=>$item->idTaskItem]);
                		}
            		}



			return $this->render('edit_task', ['task'=>$task, 'form'=>$form, 'taskItems_exist'=>$taskItems_exist, 'taskItems'=>$taskItems/*, 'work_exist'=>$work_exist, 'workItems'=>$workItems, 'workItems_exist'=>$workItems_exist*/]);
		}
	}

	public function actionEdittaskitem($idtaskitem=null){
		if (isset($_SESSION['auth'])){
			$item = TaskItem::findOne($idtaskitem);
			$form = new EditTaskItemForm();

			if($form->load(Yii::$app->request->post()) && Yii::$app->request->post('save_taskitem_edittaskitem_'.$item['idTaskItem']) && $form->validate()){
                		$item->comment = Html::encode($form->item_comment);
                		$item->save();
                		return $this->redirect(['edittask', 'idtask'=>$item['idTask']]);
            		}
			if(Yii::$app->request->post('delete_taskitem_edittaskitem_'.$item['idTaskItem'])){
                		$item->isDeleted = 1;
                		$item->save();
				$task = Task::findOne($item['idTask']);
				$task['filesQuantity'] = $task['filesQuantity']-1;
				$task->save();
                		return $this->redirect(['edittask', 'idtask'=>$item->idTask]);
            		}

			return $this->render('edit_taskitem', ['item'=>$item, 'form'=>$form]);
		}
	}

}
