<?php
   /*функции для загрузки файлов (constr.php)*/
   function myscandir($dir)
   {
	   $list=scandir($dir);
	   unset($list[0],$list[1]);
	   return array_values($list);
   }
   function clear_dir($dir) 
   {
	   $list =myscandir($dir);
	   foreach($list as $file)
	   {
		   if(is_dir($dir.$file)){
			 clear_dir($dir.$file.'/');
		     rmdir($dir.$file);
		   }
		   else{
			  unlink($dir.$file); 
		   }
	   }
   }
   /*функции для выгрузки сайта nex.php*/
   function zip($source, $destination)
     {
       if (!extension_loaded('zip') || !file_exists($source)) {
          return false;
       }
       $zip = new ZipArchive();
       if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
          return false;
       }
       $source = str_replace('\\', DIRECTORY_SEPARATOR, realpath($source));
       $source = str_replace('/', DIRECTORY_SEPARATOR, $source);
       if (is_dir($source) === true) {
         $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source),
             RecursiveIteratorIterator::SELF_FIRST);
         foreach ($files as $file) {
             $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
             $file = str_replace('/', DIRECTORY_SEPARATOR, $file);
             if ($file == '.' || $file == '..' || empty($file) || $file == DIRECTORY_SEPARATOR) {
                 continue;
             }
             // Ignore "." and ".." folders
             if (in_array(substr($file, strrpos($file, DIRECTORY_SEPARATOR) + 1), array('.', '..'))) {
                 continue;
             }
             $file = realpath($file);
             $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
             $file = str_replace('/', DIRECTORY_SEPARATOR, $file);
             if (is_dir($file) === true) {
                 $d = str_replace($source . DIRECTORY_SEPARATOR, '', $file);
                 if (empty($d)) {
                     continue;
                 }
                 $zip->addEmptyDir($d);
             } elseif (is_file($file) === true) {
                $zip->addFromString(str_replace($source . DIRECTORY_SEPARATOR, '', $file),
                    file_get_contents($file));
             } else {
                 // do nothing
             }
         }
        } elseif (is_file($source) === true) {
          $zip->addFromString(basename($source), file_get_contents($source));
     }
 
     return $zip->close();
    }
    function file_force_download($file) {
      if (file_exists($file)) {
   	    if (ob_get_level()) {
     		 ob_end_clean();
   	    }
        header('Content-Description: File Transfer');
   	    header('Content-Type: application/octet-stream');
   	    header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
   	    header('Expires: 0');
   	    header('Cache-Control: must-revalidate');
   	    header('Pragma: public');
        header('Content-Length: ' . filesize($file));
  	    readfile($file);
     }
   }
   /*функция для удаления содержимого удаляемой папки  пользователя ForAdmin.php*/
   function recursiveRemoveDir($dir) {
	    $includes = new FilesystemIterator($dir);
	    foreach ($includes as $include) {
		   if(is_dir($include) && !is_link($include)) {
			   recursiveRemoveDir($include);
		    }
		    else {
			       unlink($include);
		         }
	    }
	    rmdir($dir);
    }
