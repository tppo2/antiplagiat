<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\AuthForm;
use app\models\EditCourseForm;
use app\models\TblUser;
use app\models\Task;
use app\models\TaskItem;
use app\models\Work;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use app\models\Course;

class CourseController extends Controller
{
    public function actionCourse($id=null){
        if (isset($_SESSION['auth'])){
            if(!isset($id)){
                return $this->redirect(["auth/mainpage"]);
            }

            $course = Course::findOne($id);
            $task_exist = Task::find()->where(['idCourse'=>$course['idCourse'], 'isDeleted'=>0])->exists();
            $tasks = Task::find()->where(['idCourse'=>$course['idCourse'], 'isDeleted'=>0])->all();
            $versions = [];
            $color = [];
            $percent = [];

          	$result = [];
            if(isset($_SESSION['status'])){
                $usr = TblUser::findOne(['login'=>$_SESSION['auth']]);
                foreach($tasks as &$row){
                    if(Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$usr['idUser']])->exists()){ 
                        $version = Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$usr['idUser']])->count();
                        $versions[$row['idTask']] = $version;
                        $last_version = Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$usr['idUser']])->orderBy(['version' => SORT_DESC])->one();
                        if($last_version['uniqueness'] >=$row['uniqueness']){
                        	$result[$row['idTask']] = 'Сдано('.$last_version['uniqueness'].'%)';
                            $color[$row['idTask']] = 'green';
                        }
                        elseif($last_version['uniqueness'] < $row['uniqueness'] && $last_version['uniqueness'] >=0){
                            $color[$row['idTask']] = 'red';
                            $result[$row['idTask']] = 'Не сдано('.$last_version['uniqueness'].'%)';
                        }
                        elseif($last_version['uniqueness'] ==-1){
                        	$result[$row['idTask']] = "Проверяется";
                            $color[$row['idTask']] = 'yellow';
                        }
                    }
                    else {
                        $versions[$row['idTask']] = 0;
                        $result[$row['idTask']] = "Работа не сдана";
                        $color[$row['idTask']] = 'grey';
                    }
                }
            }
            else{
                foreach($tasks as &$row){
                    $studentsWorks = [];
                    $students = TblUser::find()->where(['isTeacher'=>False])->all();
                    foreach($students as &$student){
                        if(Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$student['idUser']])->exists()){
                            //$studentsWorks[$student['idUser']] = Work::findOne(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$student['idUser']])->max('Date');
                            $studentsWorks[$student['idUser']] = Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask'], 'idUser'=>$student['idUser']])->orderBy(['version' => SORT_DESC])->one();
                        }
                    }
                    //var_dump($studentsWorks);
                    //die();
                    $countOfWorks = count($studentsWorks);
                    $countUniqWorks = 0;
                    foreach($studentsWorks as &$works){
                        if($works['uniqueness']>=$row['uniqueness']){
                            $countUniqWorks ++;
                        }
                    }
                    $versions[$row['idTask']]= Work::find()->where(['isDeleted'=>0, 'idTask'=>$row['idTask']])->count();
                    if ($countOfWorks == 0){
                        $percent[$row['idTask']] = 100;
                    }
                    else{
                        $percent[$row['idTask']] = ($countUniqWorks/$countOfWorks)*100;
                    }
                }
            }
            if(Yii::$app->request->post('go_to_the_mainpage')){
                return $this->redirect(array('auth/mainpage'));
            }
	        if(Yii::$app->request->post('delete_course_course_'.$course['idCourse'])){
                $course -> isDeleted = 1;
                $course -> save();
                if (Task::find()->where(['idCourse'=>$course['idCourse']])->exists()){
                    $tasks = Task::find()->where(['idCourse'=>$course['idCourse']])->all();
                    Task::updateAll(['isDeleted' => 1], ['idCourse'=>$course['idCourse']]);
                    foreach($tasks as &$task){
                        TaskItem::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
                        if(Work::find()->where(['idTask'=>$task['idTask']])->exists()){
                            $works = Work::find()->where(['idTask'=>$task['idTask']])->all();
                            Work::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
                            foreach($works as &$work){
                                WorkItem::updateAll(['isDeleted' => 1], ['idWork'=>$work['idWork']]);
                            }
                        }
                    }
                }
                return $this->redirect(array('auth/mainpage'));
            }
            if(Yii::$app->request->post('edit_course_course_'.$course['idCourse'])){
                return $this->redirect(array('editcourse','id'=>$course['idCourse']));
            }
            if(Yii::$app->request->post('open_report_course')){
                return $this->redirect(array('report/report','id'=>$course['idCourse']));
            }
            foreach($tasks as &$tsk){
                if(Yii::$app->request->post('open_task_course_'.$tsk->idTask)){
                    return $this->redirect(array('task/task','idtask'=>$tsk['idTask']));
                }
            }

            return $this->render("course", compact('course', 'tasks', 'task_exist', 'versions','color', 'percent', 'result'));
        }
        else{
			return $this->redirect(['auth/authr']);
		}	
    }
    
    public function actionEditcourse($id = null){
        if (isset($_SESSION['auth'])){
            if(!isset($id)){
                return $this->redirect(["auth/mainpage"]);
            }
            $form = new EditCourseForm();
            $course = Course::findOne($id);
            $task_exist = Task::find()->where(['idCourse'=>$course['idCourse'], 'isDeleted'=>0])->exists();
            $tasks = Task::find()->where(['idCourse'=>$course['idCourse'], 'isDeleted'=>0])->all();

            if(Yii::$app->request->post('delete_course_editcourse_'.$course['idCourse'])){
                $course -> isDeleted = 1;
                $course -> save();
                if (Task::find()->where(['idCourse'=>$row['idCourse']])->exists()){
                    $tasks = Task::find()->where(['idCourse'=>$row['idCourse']])->all();
                    Task::updateAll(['isDeleted' => 1], ['idCourse'=>$row['idCourse']]);
                    foreach($tasks as &$task){
                        TaskItem::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
                        if(Work::find()->where(['idTask'=>$task['idTask']])->exists()){
                            $works = Work::find()->where(['idTask'=>$task['idTask']])->all();
                            Work::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
                            foreach($works as &$work){
                                WorkItem::updateAll(['isDeleted' => 1], ['idWork'=>$work['idWork']]);
                            }
                        }
                    }
                }
                return $this->redirect(array('auth/mainpage'));
            }

            if(Yii::$app->request->post('add_lab_'.$course['idCourse'])){
                $task = new Task();
                $task->idCourse = $course['idCourse'];
                $task->task = "Название задания";
                $task->comment = "Описание";
                $task->uniqueness = 0;
                $task->isDeleted = 0;
                $task->year = 3;
                $task->tries = 0;
                $task->filesQuantity = 0;
                $error = $task->save();
                if (!file_exists(yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask))
                {
                    mkdir(yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask);
                }

                if (!file_exists(yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/teacher_files'))
                {
                    mkdir(yii::$app->basePath.'/uploads/' . $task->idCourse . '/' . $task->idTask . '/teacher_files');
                }


                return $this->refresh();
            }

            if(Yii::$app->request->post('go_to_the_course')){
                return $this->redirect(array('course', 'id'=>$id));
            }

            if($form->load(Yii::$app->request->post()) && Yii::$app->request->post('save_course_editcourse_'.$course['idCourse']) && $form->validate()){
                $course->name = Html::encode($form->course_name);
                $course->info = Html::encode($form->course_info);
                $course ->save();
                return $this->redirect(array('course'));
            }


            foreach($tasks as &$tsk){
                if(Yii::$app->request->post('open_task_editcourse_'.$tsk->idTask)){
                    return $this->redirect(array('task/task','idtask'=>$tsk->idTask));
                }
            }


            return $this->render("edit_course", compact('form','course', 'tasks', 'task_exist'));        
        }
        else{
			return $this->redirect(['auth/authr']);
		}	
    }
}