<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\AuthForm;
use app\models\User;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use app\models\Course;
use app\models\TblUser;
use app\models\TaskItem;
use app\models\Work;
use app\models\WorkItem;
use app\models\Task;

class AuthController extends Controller
{

	public function actionExit(){
		if(session_status() === PHP_SESSION_ACTIVE){
			session_destroy();
		}
		return $this->redirect(['authr']);
	}
	public function actionAuthr()
    	{	
		if(session_status() === PHP_SESSION_ACTIVE){
    			session_destroy();
		}
		//session_destroy();
		$form = new AuthForm();
		
		if ($form->load(Yii::$app->request->post())){
			$out = $form->login();
			$login = Html::encode($form->login);
			if ($out){
				$output = explode( ' ', $form->output);
				//var_dump($output);die();
				if ($output[0] = 'True'){
					session_start();
					if($output[1] == "True"){
						$_SESSION['status'] = true;
					}
					if(!TblUser::find()->where(['login'=>$login])->exists()){
						$usr = new TblUser();
						$usr -> login = $login;
						if($output[1] == 'False'){
							$usr -> isTeacher = true;
						}
						else{
							$usr -> isTeacher = false;
						}
						$usr -> FIO = $output[2].' '.$output[3].' '.$output[4];
						$usr->save();
					}
					else{
						$usr = TblUser::findOne(['login'=>$login]);
						if ($usr->isTeacher === 1){
							unset($_SESSION['status']);
						}
					}		
					$_SESSION['auth'] = $login;
					$_SESSION['name'] = $output[2].' '.$output[3].' '.$output[4];
					return $this->redirect(["mainpage"]);
				}
			}
		}
		else{
			$output = '1';
		}
		return $this->render('authr', compact('form'));
        }
	public function actionMainpage(){
	       if (isset($_SESSION['auth'])){
			$Courses = Course::find()->where(['isDeleted' => 0])->all();
			if(Yii::$app->request->post('addCourse')){
				$course = new Course();
				$course -> name = "Название курса";
				$course -> info = "Описание курса";
				$course -> isDeleted = 0;
				$course -> save();
				if (!file_exists( yii::$app->basePath.'/uploads/' . $course->idCourse))
				{
					mkdir(yii::$app->basePath.'/uploads/' . $course->idCourse);
				}

				return $this->redirect(array('course/editcourse','id' => $course['idCourse']));
			}
			foreach($Courses as &$row){
				if(Yii::$app->request->post('delete_course_main_'.$row['idCourse'])){
					$row->isDeleted = 1;
					$row -> save();
					if (Task::find()->where(['idCourse'=>$row['idCourse']])->exists()){
						$tasks = Task::find()->where(['idCourse'=>$row['idCourse']])->all();
						Task::updateAll(['isDeleted' => 1], ['idCourse'=>$row['idCourse']]);
						foreach($tasks as &$task){
							TaskItem::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
							if(Work::find()->where(['idTask'=>$task['idTask']])->exists()){
								$works = Work::find()->where(['idTask'=>$task['idTask']])->all();
								Work::updateAll(['isDeleted' => 1], ['idTask'=>$task['idTask']]);
								foreach($works as &$work){
									WorkItem::updateAll(['isDeleted' => 1], ['idWork'=>$work['idWork']]);
								}
							}
						}
					}
					return $this -> refresh();
				}
				else if(Yii::$app->request->post('edit_course_main_'.$row['idCourse'])){
					return $this->redirect(array('course/editcourse','id' => $row['idCourse']));
				}
				else if(Yii::$app->request->post('open_course_main_'.$row['idCourse'])){
					return $this->redirect(array('course/course', 'id'=>$row['idCourse']));
				}
			}
			return $this->render("main_page", compact('Courses'));
		}
		else{
			return $this->redirect(['authr']);
		}	
	       
	}
}
