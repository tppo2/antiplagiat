<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\AuthForm;
use app\models\EditCourseForm;
use app\models\TblUser;
use app\models\Task;
use app\models\Work;
use yii\widgets\LinkPager;
use yii\helpers\Html;
use app\models\Course;

class ReportController extends Controller
{
	public function actionReport($id = null){
        if (isset($_SESSION['auth'])){
            if(!isset($id)){
                return $this->redirect(["auth/mainpage"]);
            }
            $course = Course::findOne(['isDeleted'=>0, 'idCourse'=>$id]);
            $works = [];
            $usernames = [];
            $tasknames = [];
            $allUsers = TblUser::find()->where(['isTeacher'=>False])->all();
            $tasks = Task::find()->where(['isDeleted'=>0, 'idCourse'=>$id])->all();
            foreach ($allUsers as &$user){
                foreach ($tasks as &$task){
                    if(Work::find()->where(['isDeleted'=>0, 'idUser'=>$user['idUser'], 'idTask'=>$task['idTask']])->exists()){
                        $works[$user['idUser']][$task['idTask']] = Work::find()->where(['isDeleted'=>0, 'idTask'=>$task['idTask'], 'idUser'=>$user['idUser']])->orderBy(['version' => SORT_DESC])->one();
                        $usernames[$user['idUser']] = $user['login'];
                        $tasknames[$task['idTask']] = $task['task'];
                    }
                }
            }
            return $this->render("report", compact('course', 'works', 'usernames','tasknames'));
        }
        else{
			return $this->redirect(['auth/authr']);
		}
    }

    public function actionReporttask($idTask=null){
    	if (isset($_SESSION['auth'])){
            if(!isset($idTask)){
                return $this->redirect(["auth/mainpage"]);
            }
            $tsk = Task::findOne(['isDeleted'=>0, 'idTask'=>$idTask]);
            $works = Work::find()->where(['isDeleted'=>0, 'idTask'=>$idTask])->all();
            $usernames = [];
            $allUsers = TblUser::find()->all();
            foreach ($allUsers as &$user){
            	$usernames[$user['idUser']] = $user['login'];

            	if(Yii::$app->request->post('open_report_by_name_'.$user->idUser)){
                    return $this->redirect(array('reportstudent','idUser'=>$user['idUser']));
                }
            }

            foreach ($works as &$work) {
                if(Yii::$app->request->post('download_zip_'.$work['idWork'])){
                    $path = __DIR__ . '/../uploads/' . $tsk['idCourse'] . '/' . $tsk['idTask'] . '/' . $usernames[$work['idUser']] . '/' . $work['version'];
                    if (file_exists($path.'/compresed.zip'))
                        unlink($path.'/compresed.zip');
                    $this->zip($path, $path.'/compresed.zip');
                    $this->file_force_download($path.'/compresed.zip');
                    unlink($path.'/compresed.zip');
                    return 5;
                }
            }
            return $this->render("report_task", compact('tsk', 'works', 'usernames'));
        }
        else{
			return $this->redirect(['auth/authr']);
		}	
    }

	public function actionReportstudent($idUser = null){
        if (isset($_SESSION['auth'])){
            if(!isset($idUser)){
                return $this->redirect(["auth/mainpage"]);
            }
            $works = Work::find()->where(['isDeleted'=>0, 'idUser'=>$idUser])->all();
            $coursenames = [];
            $tasknames = [];
            $usr = TblUser::findOne(['idUser'=>$idUser]);
            foreach ($works as &$work) {
            	$task = Task::findOne(['isDeleted'=>0, 'idTask'=>$work['idTask']]);
            	$course = Course::findOne(['isDeleted'=>0, 'idCourse'=>$task['idCourse']]);

                if(Yii::$app->request->post('download_zip_'.$work['idWork'])){
                    $path = __DIR__ . '/../uploads/' . $task['idCourse'] . '/' . $task['idTask'] . '/' . $usr['login'] . '/' . $work['version'];
                    if (file_exists($path.'/compresed.zip'))
                        unlink($path.'/compresed.zip');
                    $this->zip($path, $path.'/compresed.zip');
                    $this->file_force_download($path.'/compresed.zip');
                    unlink($path.'/compresed.zip');
                    return 5;
                }

            	$coursenames[$work['idTask']] = $course['name'];
            	$tasknames[$work['idTask']] = $task['task'];
            }

            return $this->render("report_student", compact('usr', 'works', 'tasknames','coursenames'));
        }
        else{
			return $this->redirect(['auth/authr']);
		}	
    }




     function zip($source, $destination)
     {
       if (!extension_loaded('zip') || !file_exists($source)) {
          return false;
       }
       $zip = new \ZipArchive();
       if (!$zip->open($destination, \ZIPARCHIVE::CREATE)) {
          return false;
       }
       $source = str_replace('\\', DIRECTORY_SEPARATOR, realpath($source));
       $source = str_replace('/', DIRECTORY_SEPARATOR, $source);
       if (is_dir($source) === true) {
         $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source),
             \RecursiveIteratorIterator::SELF_FIRST);
         foreach ($files as $file) {
             $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
             $file = str_replace('/', DIRECTORY_SEPARATOR, $file);
             if ($file == '.' || $file == '..' || empty($file) || $file == DIRECTORY_SEPARATOR) {
                 continue;
             }
             // Ignore "." and ".." folders
             if (in_array(substr($file, strrpos($file, DIRECTORY_SEPARATOR) + 1), array('.', '..'))) {
                 continue;
             }
             $file = realpath($file);
             $file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
             $file = str_replace('/', DIRECTORY_SEPARATOR, $file);
             if (is_dir($file) === true) {
                 $d = str_replace($source . DIRECTORY_SEPARATOR, '', $file);
                 if (empty($d)) {
                     continue;
                 }
                 $zip->addEmptyDir($d);
             } elseif (is_file($file) === true) {
                $zip->addFromString(str_replace($source . DIRECTORY_SEPARATOR, '', $file),
                    file_get_contents($file));
             } else {
                 // do nothing
             }
         }
        } elseif (is_file($source) === true) {
          $zip->addFromString(basename($source), file_get_contents($source));
     }
 
     return $zip->close();
    }
    function file_force_download($file) {
      if (file_exists($file)) {
        if (ob_get_level()) {
             ob_end_clean();
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        readfile($file);
     }
   }

}