<?php
$this->title="Отчёт";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h1>Отчёт по заданию: <?= $tsk["task"] ?></h1>
<?php $f = ActiveForm::begin() ?>
<table class='table'>
        <thead>
            <tr><th>Студент</th><th>Номер попытки</th><th>Дата загрузки версии</th><th>Скачать версию</th><th>Результат</th></tr>
        </thead>
        <tbody>
        <?php
        	foreach ($works as &$work) {
        		echo '<tr><td>'.Html::submitButton($usernames[$work['idUser']],['name'=>'open_report_by_name_'.$work['idUser'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
        		<td>'.$work['version'].'</td>
                <td>'.$work['date'].'</td>
                <td>'.Html::submitButton('скачать',['name'=>'download_zip_'.$work['idWork'], 'value' => 'addafs', 'class' => 'submit_text']).'</td>
                <td>'.$work['uniqueness'].'</td></tr>';
        	}
        ?>
        </tbody>
</table>
<?php ActiveForm::end() ?>