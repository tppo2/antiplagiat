<?php
$this->title="Отчёт";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h1>Отчёт по курсу: <?= $course["name"] ?></h1>
<table class='table'>
        <thead>
            <tr><th>Задание</th><th>Студент</th><th>Дата</th><th>Количество попыток</th><th>Результат</th></tr>
        </thead>
        <tbody>
        <?php
            foreach($works as &$row){
                foreach($row as &$work){
                    echo '<tr><td>'.$tasknames[$work['idTask']].'</td><td>'.$usernames[$work['idUser']].'</td>
                    <td>'.$work['date'].'</td><td>'.$work['version'].'</td><td>'.$work['uniqueness'].'</td></tr>';
                }
            }
        ?>
        </tbody>
</table>