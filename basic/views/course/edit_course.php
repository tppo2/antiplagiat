<?php
$this->title="Редактирование курса";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $f = ActiveForm::begin() ?>
<?=Html::submitButton('☚ На страницу курса',['name'=>'go_to_the_course', 'value' => 'add', 'class' => 'btn btn-primary'])?><br><br><br>
<?php ActiveForm::end() ?>
<div>
    <?php $f = ActiveForm::begin() ?>
    <?=$f->field($form, 'course_name')->textInput([ 'class'=>'input_edit element-inline','value'=>$course['name'], 'placeholder'=>'Название курса'])->label(false)?>
    <?=$f->field($form, 'course_info')->textArea([ 'class'=>'info_textarea','value'=>$course['info'], 'placeholder'=>'Описание курса'])->label(false)?>
    <hr>
    <div class ="laboratornye">
    <table class="table">
        <thead>
        <tr><th>Лабораторные работы</th></tr>
        </thead>
        <tbody>
         <?php
            if ($task_exist)
                foreach($tasks as &$tsk){
                     echo '<tr><td>
                     '.Html::submitButton($tsk->task,['name'=>'open_task_editcourse_'.$tsk['idTask'], 'value' => 'add', 'class' => 'submit_text']).'
                     </td>
                     </tr>';
                }
         ?>
        </tbody>
    </table>
    <?=Html::submitButton('Добавить',['name'=>'add_lab_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    </div>
    <br><br>
    <?=Html::submitButton('Сохранить',['name'=>'save_course_editcourse_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    <?=Html::submitButton('Удалить курс',['name'=>'delete_course_editcourse_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>
    <?php ActiveForm::end() ?>
</div>
