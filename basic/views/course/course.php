<?php
$this->title="Страница курса";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $f = ActiveForm::begin() ?>
<?=Html::submitButton('☚ На главную',['name'=>'go_to_the_mainpage', 'value' => 'add', 'class' => 'btn btn-primary'])?><br><br><br>
<?php ActiveForm::end() ?>
<div>
	<p class="element-inline"><?= $course["name"] ?></p>
    <?php if(!isset($_SESSION['status'])){?>
        <?php $f = ActiveForm::begin() ?>
        <?=Html::submitButton('Редактировать курс',['name'=>'edit_course_course_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>
        <?=Html::submitButton('Удалить курс',['name'=>'delete_course_course_'.$course['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right right_margin'])?>
        <?php ActiveForm::end() ?>
    <?php } ?>
	<br><br>
	<div class="info-style"><?= $course["info"] ?></div> 
    <hr>
    <?php $f = ActiveForm::begin() ?>
    <table class="table">
         <?php
            if(!isset($_SESSION['status'])){ 
                echo '<thead>
                <tr><th>Лабораторные работы</th><th>Суммарное количество попыток</th><th>Процент сдавших</th></tr>
                </thead>
                <tbody>';
                if ($task_exist){
                    foreach($tasks as &$tsk){
                        echo '<tr><td>
                        '.Html::submitButton($tsk->task,['name'=>'open_task_course_'.$tsk['idTask'], 'value' => 'add', 'class' => 'submit_text']).'
                        </td>
                        <td>'.$versions[$tsk['idTask']].'</td><td>'.$percent[$tsk['idTask']].'</td></tr>';
                    }
                }
            }
            else{
                echo '<thead>
                <tr><th>Лабораторные работы</th><th>Количество попыток</th><th>Результат</th></tr>
                </thead>
                <tbody>';
                if ($task_exist){
                    foreach($tasks as &$tsk){
                        echo '<tr><td>
                        '.Html::submitButton($tsk->task,['name'=>'open_task_course_'.$tsk['idTask'], 'value' => 'add', 'class' => 'submit_text']).'
                        </td>
                        <td style="background-color:'.$color[$tsk['idTask']].';color:white;">'.$versions[$tsk['idTask']].'</td>
                        <td style="background-color:'.$color[$tsk['idTask']].';color:white;">'.$result[$tsk['idTask']].'</td></tr>';
                    }
                }
            }
         ?>
        </tbody>
    </table><br><br>
    <?php if(!isset($_SESSION['status'])){?>
    <?=Html::submitButton('Получить отчёт',['name'=>'open_report_course', 'value' => 'add', 'class' => 'btn btn-primary element-right'])?>
    <?php }?>
    <?php ActiveForm::end() ?>
</div>
