<?php
$this->title="Страница редатикрования задания";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Task;
use app\models\TaskItem;


?>
<button style="float:left">☚ На главную</button><br><br><br>

<?php $f = ActiveForm::begin() ?>
    <?=$f->field($form, 'item_comment')->textArea([ 'class'=>'info_textarea','value'=>$item['comment'], 'placeholder'=>'Описание'])->label(false)?>
    <hr>

    <?=Html::submitButton('Сохранить',['name'=>'save_taskitem_edittaskitem_'.$item['idTaskItem'], 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    <?=Html::submitButton('Удалить',['name'=>'delete_taskitem_edittaskitem_'.$item['idTaskItem'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>

    <br><br>
<?php ActiveForm::end() ?>

