<?php
$this->title="Страница задания";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use app\models\UploadFile;
use yii\web\UploadedFile;
?>
<?php $f = ActiveForm::begin() ?>
<?=Html::submitButton('☚ На страницу курса',['name'=>'go_to_the_course', 'value' => 'add', 'class' => 'btn btn-primary'])?><br><br><br>
<?php ActiveForm::end() ?>
<h1><?= $task["task"] ?></h1>

<div>
	<?php if(!isset($_SESSION['status'])){?>
	<?php $f = ActiveForm::begin() ?>
		<?=Html::submitButton('Редактировать задание',['name'=>'edit_task_task_'.$task['idTask'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>
    		<?=Html::submitButton('Удалить задание',['name'=>'delete_task_task_'.$task['idTask'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right right_margin'])?>
	<?php ActiveForm::end() ?>
	<?php } ?>
	<br><br>
	<div class="info-style"><?= $task["comment"] ?></div>
    <hr>
	
	<?php // only student 
	?>	
	<?php $f = ActiveForm::begin() ?>
		<div>
			<h2>Ограничения</h2>
			<p>Уникальность: <?= $task["uniqueness"] ?>%</p>
			<?php
				if($task["tries"] == 0) {$tries = 'не ограничено';} else {$tries = $task["tries"];} 
			?>
			<p>Количество попыток: <?= $tries ?></p>
		</div>
	<?php ActiveForm::end() ?>
	<?php $f = ActiveForm::begin() ?>
		<table class="table">
        	<thead>
        		<tr><th>123</th></tr>
        	</thead>
        	<tbody>
         		<?php
				$i = 0;
				if ($taskItems_exist){
                			foreach($taskItems as $item){
                     				echo '<tr><td>'. $item["num"] .'</td><td>'. $f->field($uf, 'file1['.$i.']')->fileInput(['maxlength' => true]) .'</td><td>'. $item["comment"] .'</td></tr>';
						$i++;
                			}
				}
         		?>
        	</tbody>
    	</table>
	<?= Html::submitButton('Загрузить',['name'=>'upload_taskItems'.$task['idTask'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right right_margin'])?>
	<?php if(isset($_SESSION['error'])){echo '<p style="color:red">'.$_SESSION['error'].'</p>'; unset($_SESSION['error']);}?>
	<br><br>

	<?php ActiveForm::end() ?>



	<?php // only teacher 
	?>
<?php $f = ActiveForm::begin() ?>
		<table class="table">
        	<thead>
        		<tr><th>Загруженные решения</th></tr>
        	</thead>
        	<tbody>
         		<?php
				if ($workItems_exist){
                			foreach($workItems as &$item){
                     				echo '<tr><td>'.Html::submitButton($item->file,['name'=>'download_file_'.$item['idWorkItem'], 'value' => '12412', 'class' => 'submit_text']).'</td></tr>';
                			}
				}
         		?>
        </tbody>
    </table><br><br>
<?php ActiveForm::end() ?>
<?php if(!isset($_SESSION['status'])){?>
<?php $f = ActiveForm::begin() ?>
 <?=Html::submitButton('Получить отчёт',['name'=>'open_report_task', 'value' => 'add512', 'class' => 'btn btn-primary element-right'])?>
	<?php ActiveForm::end() ?>
<?php } ?>
</div>
