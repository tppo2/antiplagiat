<?php
$this->title="Страница редактирования задания";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Task;
use app\models\TaskItem;


?>
<?php $f = ActiveForm::begin() ?>
<?=Html::submitButton('☚ На страницу задания',['name'=>'go_to_the_task', 'value' => 'add', 'class' => 'btn btn-primary'])?><br><br><br>
<?php ActiveForm::end() ?>

<?php $f = ActiveForm::begin(['id'=>'edit-task-form']) ?>
    <?=$f->field($form, 'task_name')->textInput([ 'class'=>'input_edit element-inline','value'=>$task['task'], 'placeholder'=>'Название задания'])->label(false)?>
    <?=$f->field($form, 'task_comment')->textArea([ 'class'=>'info_textarea','value'=>$task['comment'], 'placeholder'=>'Описание'])->label(false)?>
    <hr>
    <h1>Settings</h1>
    <?=$f->field($form, 'task_uniqueness')->textInput([ 'class'=>'input_edit element-inline','value'=>$task['uniqueness'], 'placeholder'=>'Уникальность'])->label('Уникальность', ['class'=>'label-class'])?>
    <?=$f->field($form, 'task_tries')->textInput([ 'class'=>'input_edit element-inline','value'=>$task['tries'], 'placeholder'=>'Кол-во попыток'])->label('Кол-во попыток', ['class'=>'label-class'])?>
    <?=$f->field($form, 'task_years')->textInput([ 'class'=>'input_edit element-inline','value'=>$task['year'], 'placeholder'=>'Период (в годах)'])->label('Период (в годах)', ['class'=>'label-class'])?>

    <?=Html::submitButton('Сохранить',['name'=>'save_task_edittask_'.$task['idTask'], 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    <?=Html::submitButton('Удалить задание',['name'=>'delete_task_edittask_'.$task['idTask'], 'value' => 'add', 'class' => 'btn btn-primary element-inline element-right'])?>

	<br><br>
    <div class ="laboratornye">
    <table class="table">
        <thead>
        <tr><th>Номер</th><th>Описание</th></tr>
        </thead>
        <tbody>
         <?php
            if ($taskItems_exist){
                foreach($taskItems as &$item){
                     	echo '<tr><td>'.$item['num'].'</td><td>
                     		'.Html::submitButton($item->comment,['name'=>'open_item_edittask_'.$item['idTaskItem'], 'value' => 'add', 'class' => 'submit_text']).'
                     		</td></tr>';
                }
	    }
         ?>
        </tbody>
    </table>
    <?=Html::submitButton('Добавить',['name'=>'add_item_'.$task['idTask'], 'value' => 'add', 'class' => 'btn btn-primary element-inline'])?>
    </div>
    <br><br>
<?php ActiveForm::end() ?>

