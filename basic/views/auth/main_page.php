<?php
$this->title="Главная страница";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h1>Курсы</h1><br>
<div id="courses-scroll" style="overflow: auto; margin-left: 50px; height: 400px;">
<?php $f = ActiveForm::begin() ?>
	<?php
	foreach($Courses as &$row){
    	echo '<div class="one-course">

    	<div class="col-log-3">
    	'.Html::submitButton($row['name'],['name'=>'open_course_main_'.$row['idCourse'], 'value' => 'add', 'class' => 'submit_text']).'<br><br>';
		if(!isset($_SESSION['status'])){
			echo Html::submitButton('Редактировать',['name'=>'edit_course_main_'.$row['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary']).'<br><br>
			'.Html::submitButton('Удалить',['name'=>'delete_course_main_'.$row['idCourse'], 'value' => 'add', 'class' => 'btn btn-primary']);
		}
		echo '</div>
    	</div><br>';
	}	
	?>
<?php ActiveForm::end() ?>
</div><br>
<div>
<a href="#" style="float:left" id="show-all" onclick="showAllCourses()">Показать все курсы</a><br><br>
<?php if(!isset($_SESSION['status'])){ ?>
	<?php $f = ActiveForm::begin() ?>
    	<?= Html::submitButton('Добавить курс',['name'=>'addCourse', 'value' => 'add', 'class' => 'btn btn-primary']);?>
	<?php ActiveForm::end() ?>
<?php } ?>
</div>