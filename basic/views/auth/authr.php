<?php
$this->title="Авторизация";
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="auth row">
	<h1>СИСТЕМА АНАЛИЗА СТУДЕНЧЕСКИХ РАБОТ</h1><br><br><br><br>
	<div class="col-lg-4">
		</div>
	<div class="col-lg-4">
<?php $f=ActiveForm::begin(['id'=>'auth-form']);?>
	<?=$f->field($form, 'login')->textInput()->input('textInput', ['placeholder' => "Логин"])->label(false)?>
	<?=$f->field($form, 'pass')->passwordInput()->input('password', ['placeholder' => "Пароль"])->label(false)?>
	<?=Html::submitButton('Отправить');?>
<?php ActiveForm::end();?>
</div>
<div class="col-lg-4">
	</div>
</div>
