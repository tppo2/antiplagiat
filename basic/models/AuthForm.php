<?php
namespace app\models;

use Yii;
use yii\base\Model;

class AuthForm extends Model
{
	public $login;
	public $pass;

	public $output;

	public function rules()
	{
		return [
		       [['login','pass'],'required'],
		];
	}

	public function login(){
		if ($this->validate()){
			$login = $this->login;
	        $password = $this->pass;
			$cmd = "python3 ".Yii::$app->basePath."/common/python/p.py $login $password";
			$this->output = shell_exec(escapeshellcmd($cmd));
			if ($this->output == NULL)
				return false;
			//var_dump($this->output);
			$out = explode( ' ', $this->output);
			//var_dump($out);die();
			if ($out[0] == 'False')
				return false;
			//var_dump($this->output);die();
			return true;
		}
		else {
			return false;
		}
	}
}
?>
