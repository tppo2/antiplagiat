<?php
namespace app\models;

use Yii;
use yii\base\Model;

class EditCourseForm extends Model
{
	public $course_name;
	public $course_info;

	public function rules()
	{
		return [
		       [['course_name', 'course_info'],'required'],
		];
	}
}
?>
