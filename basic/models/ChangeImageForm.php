<?php
namespace app\models;

use Yii;
use yii\base\Model;

class ChangeImageForm extends Model
{
	public $name;
	public $id;
	public $caption;

	public function rules()
	{
		return [
		       [['id','name','caption'],'required'],
		];
	}
}
?>
